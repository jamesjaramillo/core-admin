package pe.com.hiper.DTO;

public class MontoPagadoDTO {
	
	private Long idMontoPagado;
	private String cTrace;
	private int nDenominacion;
	private int nTipo;
	private int bpayout;
	
	public Long getIdMontoPagado() {
		return idMontoPagado;
	}
	public void setIdMontoPagado(Long idMontoPagado) {
		this.idMontoPagado = idMontoPagado;
	}
	public String getcTrace() {
		return cTrace;
	}
	public void setcTrace(String cTrace) {
		this.cTrace = cTrace;
	}
	public int getnDenominacion() {
		return nDenominacion;
	}
	public void setnDenominacion(int nDenominacion) {
		this.nDenominacion = nDenominacion;
	}
	public int getnTipo() {
		return nTipo;
	}
	public void setnTipo(int nTipo) {
		this.nTipo = nTipo;
	}
	public int getBpayout() {
		return bpayout;
	}
	public void setBpayout(int bpayout) {
		this.bpayout = bpayout;
	}

	

}

package pe.com.hiper.model;

import java.util.ArrayList; 
import java.util.HashMap;
import java.util.List;

/**
 * Clase para definir la moneda representativa
 * 
 * @author richard
 *
 */
public class Moneda {
	private int denominacion;
	private int cantidad;
	private int estado;
	private String descripcion;
	private String idNote;
	private int cantidadPayout;
	private int cantidadCashbox;
	private int cantidadDispensada;
	private int cantidadNoDispensada;
	private HashMap<Integer, Integer> billetesCashBox;
	// private List<String> DatosBilletes=new ArrayList<String>();
	private List<Moneda> billetes = new ArrayList<Moneda>();

	public Moneda() {
	}

	public Moneda(int denominacion, int cantidad) {
		super();
		this.denominacion = denominacion;
		this.cantidad = cantidad;
	}

	/**
	 * 
	 * @param denominacion
	 * @param cantidad
	 * @param descripcion
	 */
	public Moneda(int denominacion, int cantidad, String descripcion) {
		super();
		this.denominacion = denominacion;
		this.cantidad = cantidad;
		this.descripcion = descripcion;
	}

	public Moneda(int denominacion, String idNote, String descripcion) {
		super();
		this.descripcion = descripcion;
		this.denominacion = denominacion;
		this.idNote = idNote;
	}

	public int getDenominacion() {
		return denominacion;
	}

	public void setDenominacion(int denominacion) {
		this.denominacion = denominacion;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public List<Moneda> getBilletes() {
		return billetes;
	}

	public void setBilletes(List<Moneda> billetes) {
		this.billetes = billetes;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIdNote() {
		return idNote;
	}

	public void setIdNote(String idNote) {
		this.idNote = idNote;
	}

	public int getCantidadPayout() {
		return cantidadPayout;
	}

	public void setCantidadPayout(int cantidadPayout) {
		this.cantidadPayout = cantidadPayout;
	}

	public int getCantidadCashbox() {
		return cantidadCashbox;
	}

	public void setCantidadCashbox(int cantidadCashbox) {
		this.cantidadCashbox = cantidadCashbox;
	}

	public int getCantidadDispensada() {
		return cantidadDispensada;
	}

	public void setCantidadDispensada(int cantidadDispensada) {
		this.cantidadDispensada = cantidadDispensada;
	}

	public int getCantidadNoDispensada() {
		return cantidadNoDispensada;
	}

	public void setCantidadNoDispensada(int cantidadNoDispensada) {
		this.cantidadNoDispensada = cantidadNoDispensada;
	}
	

	public HashMap<Integer, Integer> getBilletesCashBox() {
		return billetesCashBox;
	}

	public void setBilletesCashBox(HashMap<Integer, Integer> billetesCashBox) {
		this.billetesCashBox = billetesCashBox;
	}

	@Override
	public String toString() {
		return "Moneda [denominacion=" + denominacion + ", cantidad=" + cantidad + ", estado=" + estado + ", descripcion=" + descripcion + ", idNote=" + idNote + ", cantidadPayout=" + cantidadPayout + ", cantidadCashbox=" + cantidadCashbox + ", cantidadDispensada=" + cantidadDispensada + ", cantidadNoDispensada=" + cantidadNoDispensada + ", billetes=" + billetes + " billetesCashBox = "+billetesCashBox+"]";
	}

}

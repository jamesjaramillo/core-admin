package pe.com.hiper.model;

public class Componente {
	private String tipo;
	private int numeroComponente;
	private String modo;
	private String descripcion;
	private String denominacion;
	private int conteo;
	private String capacidad;
	private int ingresos;
	private int dispensados;
	private int conteoInicial;
	private int conteoMaximo;
	private String status;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getNumeroComponente() {
		return numeroComponente;
	}

	public void setNumeroComponente(int numeroComponente) {
		this.numeroComponente = numeroComponente;
	}

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDenominacion() {
		return denominacion;
	}

	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}

	public int getConteo() {
		return conteo;
	}

	public void setConteo(int conteo) {
		this.conteo = conteo;
	}

	public String getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}

	public int getIngresos() {
		return ingresos;
	}

	public void setIngresos(int ingresos) {
		this.ingresos = ingresos;
	}

	public int getDispensados() {
		return dispensados;
	}

	public void setDispensados(int dispensados) {
		this.dispensados = dispensados;
	}

	public int getConteoInicial() {
		return conteoInicial;
	}

	public void setConteoInicial(int conteoInicial) {
		this.conteoInicial = conteoInicial;
	}

	public int getConteoMaximo() {
		return conteoMaximo;
	}

	public void setConteoMaximo(int conteoMaximo) {
		this.conteoMaximo = conteoMaximo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Componente [tipo=" + tipo + ", numeroComponente=" + numeroComponente + ", modo=" + modo + ", descripcion=" + descripcion + ", denominacion="
				+ denominacion + ", conteo=" + conteo + ", capacidad=" + capacidad + ", ingresos=" + ingresos + ", dispensados=" + dispensados
				+ ", conteoInicial=" + conteoInicial + ", conteoMaximo=" + conteoMaximo + ", status=" + status + "]";
	}
}

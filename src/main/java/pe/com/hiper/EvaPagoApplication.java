package pe.com.hiper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaPagoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaPagoApplication.class, args);
	}

}

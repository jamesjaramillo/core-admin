package pe.com.hiper.common;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import pe.com.hiper.entity.CBDevice;
import pe.com.hiper.entity.TmDevice;

public class CalcularVueltoBilletes {
	

	
	
	
	public List<TmDevice> calcularDenominacionBilletes(String montoMaximo, String montoIngresado, List<TmDevice>lstDispositivos) throws Exception {
		//customLogger.Info(this.getClass().getName(), "000000", "Executing metodo calcularDenominacionBilletes()");				
		double dbMontoDeuda = Double.parseDouble(montoMaximo);
		double dbMontoIngresado = Double.parseDouble(montoIngresado);
        double vueltoCalculado = dbMontoIngresado - dbMontoDeuda;
        DecimalFormat df = new DecimalFormat("#.00");
		if(vueltoCalculado<0){
			System.out.println("MONTO A INGRESADO TIENE QUE SER MAYOR AL MONTO A PAGAR");
			return null;			
		}
        
		int iDenominacion = 0;
        int iCantidad = 0;
        String iCantidadActual = "0";
        Double dvuelto = vueltoCalculado;
        
        int iVuelto = dvuelto.intValue();
        Double residuo = vueltoCalculado-iVuelto;        
        
        int iVueltoTemp = iVuelto;
        int nroBilletesParcial,nroBilletesPayoutNew;
        List<TmDevice> listaVuelto= new ArrayList<TmDevice>();
        //String dispositivo="PY";
        
        try {
        	
                CBDevice cb=new CBDevice();
                //ArrayList<CBDevice>lstDispositivos=cb.obtenerDenominacionesDispositivo(terminal, dispositivo, conn);
                if(lstDispositivos!=null && lstDispositivos.size()>0){
                	
	                	TmDevice vueltoResta=new TmDevice();
	                	vueltoResta.setcDsCodeDenomination("vuelto");
	                    if(vueltoCalculado==0){
	                    	vueltoResta.setnDsQuantityDenomination("0.00");	
	                    }else{
	                    	vueltoResta.setnDsQuantityDenomination(String.valueOf(df.format(vueltoCalculado)));
	                    }
	                    listaVuelto.add(vueltoResta);  
                    
	               for (int i = 0; i < lstDispositivos.size(); i++) {
                        TmDevice vuelto=null;  
                        //nroBilletesPayout = 0;
                        String denominacion=lstDispositivos.get(i).getcDsCodeDenomination();
                        iDenominacion = Integer.parseInt(denominacion.substring(0, denominacion.length() - 2));                
                        iCantidad = Integer.parseInt(lstDispositivos.get(i).getnDsQuantityDenomination());
                        iCantidadActual=lstDispositivos.get(i).getnDsQuantityDenomination();
                        Long M_cDsId = lstDispositivos.get(i).getcDsId();
                        nroBilletesParcial = iVueltoTemp / iDenominacion;

                        // si la cantidad de billetes que debemos devolver no corresponde a la demonicacion
                        if (nroBilletesParcial > 0) {
                            nroBilletesPayoutNew = iCantidad;

                            // si la cantidad de billetes de 200 de base de datos es mayor o igual que la del vuelto
                            if (nroBilletesPayoutNew >= nroBilletesParcial) {                                
                                vuelto=new TmDevice();
                                vuelto.setnDsQuantityDenomination(String.valueOf(nroBilletesParcial));
                                vuelto.setcDsIniDenomination(iCantidadActual);
                                vuelto.setcDsCodeDenomination(String.valueOf(iDenominacion));
                                vuelto.setcDsId(M_cDsId);
                                vuelto.setcDsMoneyCode(lstDispositivos.get(i).getcDsMoneyCode());
                                listaVuelto.add(vuelto);
  
                                // actualizamos el vuelto
                                iVueltoTemp = iVueltoTemp % iDenominacion;

                            }else if ((nroBilletesPayoutNew < nroBilletesParcial) && (nroBilletesPayoutNew > 0)) {
                                vuelto=new TmDevice();
                                vuelto.setnDsQuantityDenomination(String.valueOf(nroBilletesPayoutNew));
                                vuelto.setcDsIniDenomination(iCantidadActual);
                                vuelto.setcDsCodeDenomination(String.valueOf(iDenominacion));
                                vuelto.setcDsId(M_cDsId);
                                vuelto.setcDsMoneyCode(lstDispositivos.get(i).getcDsMoneyCode());
                                listaVuelto.add(vuelto);
                                 // actualziamos la cantidad de billete de 200 de la base de datos a cero
                                //nroBilletesPayout = 0;
                                // actualizamos el nuevo vuelto
                                iVueltoTemp = iVueltoTemp - (nroBilletesPayoutNew * iDenominacion);
                            }else {
                                System.out.println("NO HAY BILLETES DE DENOMINACION "+iDenominacion);
                            	/*vuelto=new CBDevice();
                                vuelto.setM_nDsQuantityDenomination("0");
                                vuelto.setM_cDsCodeDenomination(String.valueOf(iDenominacion));
                                listaVuelto.add(vuelto);*/
                                // actualizamos la cantidad de billetes de 200 de la base de datos con lo que envia el kiosco
                                //nroBilletesPayout = 0;
                            }

                        }else {
                           
                            nroBilletesPayoutNew = iCantidad;
                        }
                    }// fin de for
	               /*if(listaVuelto!= null && listaVuelto.size()>0){
	            	   for(int j=0;j<listaVuelto.size();j++){
	            		   if(listaVuelto.get(j).getM_cDsId()!=null){
		                   	int a=Integer.parseInt(listaVuelto.get(j).getM_cDsIniDenomination());
		                   	int b=Integer.parseInt(listaVuelto.get(j).getM_nDsQuantityDenomination());
		                   	cb.actualizaContadores(listaVuelto.get(j).getM_cDsId(), String.valueOf(a-b), conn);
	            		   }
		               	}   
	               }*/	               		              
	               
                    double vueltoFaltante=(double) iVueltoTemp;
                    if(residuo>0){
                        vueltoFaltante=vueltoFaltante+residuo;                        
                    }
                                                                               
                    TmDevice cashBack=new TmDevice();
                    cashBack.setcDsCodeDenomination("saldoMonedas");
                    if(vueltoFaltante==0){
                    	cashBack.setnDsQuantityDenomination("0.00");	
                    }else{
                    	cashBack.setnDsQuantityDenomination(String.valueOf(df.format(vueltoFaltante)));
                    }
                    listaVuelto.add(cashBack);
                }

           
        
        } catch (Exception e) {
        	System.out.println(this.getClass().getName()+" ERROR: "+e.getMessage());
        	//customLogger.Error(this.getClass().getName(), "000000","ERROR", new Error(e));
        	e.printStackTrace();
        	return null;
        }
        
        return listaVuelto;
    }
	

}

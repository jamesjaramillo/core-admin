package pe.com.hiper.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import pe.com.hiper.entity.TmDevice;
import pe.com.hiper.entity.TpMontoPagado;

public class UtilMoneda {
	
	private final static int BILLETES=1;
	private final static int MONEDAS=2;
	
	private final static int BILLETES_CASHBOX=0;
	private final int BILLETES_STACKER=1;
	
	public final static String DENO_BILLETES_PAYOUT="BILLETESPAY";
	public final static String DENO_BILLETES_CASHBOX="BILLETESBOX";
	public final static String DENO_MONEDAS="MONEDAS";
	public final static String TOTAL_INGRESADO="TOTALINGRESADO";

	/**
	 * @author rrondinel
	 * @param denominaciones
	 * Este metodo se encarga de separ los billetes ingresados para reciclar
	 * @return
	 * 
	 * 
	 * 
	 */
	public static HashMap<String, Object> obtenerDenominacionIngresada(List<TpMontoPagado> denominaciones){
		HashMap<Double, Integer> monedas = new LinkedHashMap<Double, Integer>();
		HashMap<Integer, Integer> billetes = new LinkedHashMap<Integer, Integer>();
		HashMap<Integer, Integer> billetesPayout = new LinkedHashMap<Integer, Integer>();
		
		HashMap<String, Object> resultado=new HashMap<String, Object>();
		double montoPagadoTotal=0.0;
		
		for (TpMontoPagado montoPagado : denominaciones) {
			
			BigDecimal montoPagadoTotalBIG=new BigDecimal(montoPagadoTotal + montoPagado.getnDenominacion());
			montoPagadoTotalBIG=montoPagadoTotalBIG.setScale(2,RoundingMode.HALF_UP);
			montoPagadoTotal = montoPagadoTotalBIG.doubleValue();

			if (montoPagado.getnTipo() == MONEDAS) {
				if (monedas.containsKey(montoPagado.getnDenominacion())) {
					monedas.put(montoPagado.getnDenominacion(), monedas.get(montoPagado.getnDenominacion()) + 1);
				} else {
					monedas.put(montoPagado.getnDenominacion(), 1);
				}
				
			} else if (montoPagado.getnTipo() == BILLETES) {
				if (montoPagado.getBpayout() == BILLETES_CASHBOX) {
					if (billetes.containsKey((int) Math.round(montoPagado.getnDenominacion()))) {
						billetes.put((int) Math.round(montoPagado.getnDenominacion()), billetes.get((int) Math.round(montoPagado.getnDenominacion())) + 1);
					} else {
						billetes.put((int) Math.round(montoPagado.getnDenominacion()), 1);
					}
				} else {
					if (billetesPayout.containsKey((int) Math.round(montoPagado.getnDenominacion()))) {
						billetesPayout.put((int) Math.round(montoPagado.getnDenominacion()), billetesPayout.get((int) Math.round(montoPagado.getnDenominacion())) + 1);
					} else {
						billetesPayout.put((int) Math.round(montoPagado.getnDenominacion()), 1);
					}
				}

			}
		}
		resultado.put("BILLETESPAY", billetesPayout);
		resultado.put("BILLETESBOX", billetes);
		resultado.put("MONEDAS", monedas);
		resultado.put("TOTALINGRESADO", montoPagadoTotal);
		return resultado;
	}
	
	public static String getFormatBilletes(HashMap<Integer, Integer> denominacion, List<TmDevice> listaDevice) {
		System.out.println("denominacion "+denominacion);
		String data="";
		for (TmDevice tmDevice : listaDevice) {
			
			data+=tmDevice.getcDsCodeDenomination()+";"+denominacion.getOrDefault(Integer.parseInt(tmDevice.getcDsCodeDenomination())/100, 0)+";"+tmDevice.getcDsMoneyCode()+":";
		}
		if(!data.isEmpty())
			data=data.substring(0, data.length()-1);
		return data;
	}
	
	public static String getFormatMonedas(HashMap<Double, Integer> denominacion, List<TmDevice> listaDevice) {
		System.out.println("denominacion "+denominacion);
		String data="";
		for (TmDevice tmDevice : listaDevice) {
			
			data+=tmDevice.getcDsCodeDenomination()+";"+denominacion.getOrDefault((Double)Double.parseDouble(tmDevice.getcDsCodeDenomination())/100, 0)+";"+tmDevice.getcDsMoneyCode()+":";
		}
		if(!data.isEmpty())
			data=data.substring(0, data.length()-1);
		return data;
	}

	public static String getFormatData(List<TmDevice> listaDevice) {
		String data="";
		for (TmDevice tmDevice : listaDevice) {
			
			data+=tmDevice.getcDsCodeDenomination()+";"+tmDevice.getnDsQuantityDenomination()+";"+tmDevice.getcDsMoneyCode()+":";
		}
		if(!data.isEmpty())
			data=data.substring(0, data.length()-1);
		return data;
	}
}

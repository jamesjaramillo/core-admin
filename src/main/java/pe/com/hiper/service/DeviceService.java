package pe.com.hiper.service;

import java.util.List;

import pe.com.hiper.entity.TmDevice;

public interface DeviceService {
	public List<TmDevice> obtenerDispositivos(String dispositivo);
	public TmDevice actualizarCantidad(TmDevice device);
	
	public TmDevice getDenominacionDispositivo(String deniminacion, String device);
	
	public void actualizarCantidadDenominacion(int cantidad, String denominacion, String device);
}

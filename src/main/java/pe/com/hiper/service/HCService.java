package pe.com.hiper.service;

import java.util.HashMap;


public interface HCService {
	
	public HashMap<String, String> obtenerCostoProducto(String trace, String codigoServicio, String nroCupon);
	
	public HashMap<String, String> procesarPagoTotalNuevo(double costo_servicio, double monto_pagado, String id_producto,
			String id_cliente, String deno_cant_bill, String deno_cant_mone, String trace, 
			String estado_hopper1, String estado_hopper2, String estado_hopper3, String desc_producto, 
			String agencia, String terminal, String flagTPP, String flagRecicla, String deno_cant_bill_pay, String camino,String numOperacion, String numAutorizacion, String cardNumberEnc, String PaymentTypeCode);
	
	public HashMap<String, String> recargarHopper(String denMonRecarga, String denMonRecargaSH, String denMonRecargaBI, String dataRecargaCB, String dataRecargaCBM, String cargaPapel, String opcRecaudador, String flagRetiroMon, 
			String flagRetiroBill, String trace, String terminal, int tipoRecarga, String operationType, String tipodoc, String numdoc, String usuario);
	
	public HashMap<String, String> estadoHopSMHopBill(String trace, String opcRecaudador, String flagRetiroMon, String flagRetiroBill, String terminal, String nombreRol, String numTrjUsuario);
	
	public HashMap<String, String> reportePortales(String trxCode, String tipoTrx, int numTrx, String trace, String terminal, String nombreRol, String numTrjUsuario);
	
	public HashMap<String, String> reporteUlt5TrxPortales(String trace, String terminal, String nombreRol, String numTrjUsuario);

	public HashMap<String, String> pruebaRecargaDescarga(String denMonRecarga, String denMonRecargaSH, String denMonRecargaBI, String dataRecargaCB, String dataRecargaCBMonedas, String cargaPapel, String opcRecaudador, String flagRetiroMon, 
	String flagRetiroBill, String trace, String terminal, String operationType);
}

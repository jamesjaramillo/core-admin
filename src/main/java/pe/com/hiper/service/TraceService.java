package pe.com.hiper.service;

import pe.com.hiper.entity.TpTrace;

public interface TraceService {
	
	public TpTrace save(TpTrace trace);
	
	public TpTrace getTrace();
}

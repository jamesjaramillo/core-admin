package pe.com.hiper.service;

import java.util.List;

import pe.com.hiper.entity.TpMontoPagado;

public interface MontoPagadoService {
	
	public TpMontoPagado saveMontoPagado(TpMontoPagado montoPagado);
	
	public List<TpMontoPagado> listarMontoPagado(String trace);

}

package pe.com.hiper.service;

import java.util.HashMap;
import java.util.ArrayList;

import pe.com.hiper.model.Moneda;

public interface EvaCoreService {
	
	public HashMap<String, String> serviciosMonedero(String trace, String servicio);
	
	public Moneda aceptarBilletes(String trace);

	public Moneda dispensarBilletes(String trace, ArrayList<Moneda> lsMonedas);
	
	public HashMap<String, String> finalizarScrow(String trace, boolean isCancel);
	
	public HashMap<String, String> agregarMonedasSH(String trace, int cantidad, double denominacion);
	
	public HashMap<String, String> aceptarScrow(String trace);
	
	public HashMap<String, String> devolverScrow(String trace);
	
	public HashMap<String, String> invocarServicioGenerico(String trace, String servicio);
	
	public HashMap<String, String> dispensarHopper(String trace, String direccion, int cantidad);
	
	public HashMap<String, Integer> getBilletesPorComponente(String trace);
	
	public Moneda billetesIngresados(String trace);
	

}

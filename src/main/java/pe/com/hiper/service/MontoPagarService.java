package pe.com.hiper.service;

import pe.com.hiper.entity.TpMontoPagar;

public interface MontoPagarService {
	
	public TpMontoPagar getMontoPagar(String trace);
	
	public TpMontoPagar saveMontoPagar(TpMontoPagar montoPagar);

}

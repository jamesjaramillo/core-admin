package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.business.AdminBusiness;

@RestController
@RequestMapping(path = "/evaAdmin/api/v1/admin")
public class AdminController {

	@Autowired
	@Qualifier("adminBusinessFujitsu")
	AdminBusiness adminBusiness;
	
	@Autowired
	@Qualifier("adminBusinessNv200")
	AdminBusiness adminBusinessNv200;
	
	@Value("${kiosco.tipoAceptador}")
	private String tipoAceptador;
	
	private AdminBusiness getAdmingBusiness() {
		if(tipoAceptador.equals("4")) {
			return adminBusiness;
		}
		else {
			return adminBusinessNv200;
		}
	}

	
	@PostMapping(path = "/dataAbastecimiento", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> dataAbastecimiento(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, Object> data = getAdmingBusiness().dataAbastecimiento(input.get("trace").toString(),
				Boolean.parseBoolean("manipulated"));

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/abastecimiento", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> abastecimiento(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = getAdmingBusiness().abastecimiento(input.get("denMonRecarga").toString(),
				input.get("trace").toString(), input.get("tipoDocumento").toString(),
				input.get("nroDocumento").toString(), "usuario");

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/vaciarSmartHopper", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> vaciarSmartHopper(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = getAdmingBusiness().vaciarSmartHopper(input.get("trace").toString(),
				input.get("tipoDocumento").toString(), input.get("nroDocumento").toString(), "usuario");

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/vaciarCashBoxMonedas", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> vaciarCashBoxMonedas(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = getAdmingBusiness().vaciarCashBoxMonedas(input.get("trace").toString(),
				input.get("tipoDocumento").toString(), input.get("nroDocumento").toString(), "usuario");
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/iniciarPayoutAStacker", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> iniciarPayoutAStacker(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = getAdmingBusiness().iniciarPayoutAStacker(input.get("trace").toString(),
				Boolean.parseBoolean(input.get("manipulated").toString()), input.get("tipoDocumento").toString(),
				input.get("nroDocumento").toString(), "usuario");
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/vaciarCashBox", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> vaciarCashBox(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = getAdmingBusiness().vaciarCashBox(input.get("trace").toString(),
				input.get("tipoDocumento").toString(), input.get("nroDocumento").toString(), "usuario");
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/getMontoXDispositivo", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> getMontoXDispositivo(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = getAdmingBusiness().getMontoXDispositivo(input.get("trace").toString(),
				input.get("dispositivo").toString());
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/vaciarHopper", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> vaciarHopper(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = getAdmingBusiness().vaciarHopper(input.get("trace").toString(),
				input.get("tipoDocumento").toString(), input.get("nroMonedas").toString(),
				input.get("usuario").toString());
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/generarReporte", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> generarReporte(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = getAdmingBusiness().generarReporte(input.get("trace").toString(),
				input.get("tipoTrx").toString(), input.get("tipoTransaccion").toString(),
				Integer.parseInt(input.get("cantidadTrx").toString()), input.get("tipoDocumento").toString(),
				input.get("nroDocumento").toString());
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/ingresarBillAbastecimiento", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> ingresarBillAbastecimiento(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, Object> data = getAdmingBusiness().ingresarBillAbastecimiento(input.get("trace").toString());
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/desactivarDispositivos", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> desactivarDispositivos(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = getAdmingBusiness().desactivarDispositivos(input.get("trace").toString(), input.get("accion").toString());
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/consultarEfectivoIngresado", consumes = "application/json", produces = "application/json")
	public HashMap<String, Object> consultarEfectivoIngresado(@RequestBody HashMap<String, Object> input) {
		return getAdmingBusiness().consultarEfectivoIngresado(input.get("trace").toString());
	}

	@PostMapping(path = "/tramaSMH", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> tramaSMH(@RequestBody HashMap<String, Object> input) throws Exception {
		HashMap<String, String> data = getAdmingBusiness().tramaSMH();

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
}

package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.business.HostClienteBusiness;

@RestController
@RequestMapping(path = "/evaAdmin/api/v1/hostCliente")
public class hostController {

	@Autowired
	HostClienteBusiness hostClienteBus;

	@PostMapping(path = "/obtenerCostoProducto", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> obtenerCostoProducto(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = hostClienteBus.obtenerCostoProducto(input.get("trace").toString(),
				input.get("nroTicket").toString(), input.get("nroCupon").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

}

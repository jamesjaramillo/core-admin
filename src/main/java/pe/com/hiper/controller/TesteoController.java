package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import pe.com.hiper.business.TesteoBusiness;


/**
	* APIs para el testeo de dispositivos
	* @author jamesJH
*/
@RestController
@RequestMapping(path = "/evaAdmin/api/v1/adminTesteo")
public class TesteoController {

	@Autowired
	TesteoBusiness testeoBusiness;

	@PostMapping(path = "/ingresarBilletesTest", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> ingresarBilletesTest(@RequestBody HashMap<String, Object> input) throws Exception {
		HashMap<String, Object> data = testeoBusiness.ingresarBilletesTest(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/dispensarBilletesTest", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> dispensarBilletesTest(@RequestBody HashMap<String, Object> input) throws Exception {
		HashMap<String, Object> data = testeoBusiness.dispensarBilletesTest(input.get("trace").toString(), Integer.parseInt((String)input.get("denominacion")), (int)input.get("cantidadDispensada"));

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/habilitarIngresoMonedaTest", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> habilitarIngresoMonedaTest(@RequestBody HashMap<String, Object> input) throws Exception {
		HashMap<String, String> data = testeoBusiness.habilitarIngresoMonedaTest(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/getDenominacionMonedas", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> getDenominacionMonedas(@RequestBody HashMap<String, Object> input) throws Exception {
		HashMap<String, String> data = testeoBusiness.getDenominacionMonedas(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/detenerMonederoTest", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> detenerMonederoTest(@RequestBody HashMap<String, Object> input) throws Exception {
		HashMap<String, String> data = testeoBusiness.detenerMonederoTest(input.get("trace").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/aceptarMonedasScrowTest", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> aceptarMonedasScrowTest(@RequestBody HashMap<String, Object> input) throws Exception {
		HashMap<String, String> data = testeoBusiness.aceptarMonedasScrowTest(input.get("trace").toString(), input.get("mensaje").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/devolverMonedasScrowTest", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> devolverMonedasScrowTest(@RequestBody HashMap<String, Object> input) throws Exception {
		HashMap<String, String> data = testeoBusiness.devolverMonedasScrowTest(input.get("trace").toString(), input.get("mensaje").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
}

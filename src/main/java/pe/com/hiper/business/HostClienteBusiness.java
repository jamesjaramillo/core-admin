package pe.com.hiper.business;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.service.HCService;

@Component
public class HostClienteBusiness {
	private static final Logger LOGGER = LogManager.getLogger(HostClienteBusiness.class);
	
	@Autowired
	HCService hcService;
	
	public HashMap<String, String> obtenerCostoProducto(String trace, String codigoServicio, String nroCupon) {
		LOGGER.info(trace + " INICIO habilitarIngresoEfectivo");
		HashMap<String, String> data = new HashMap<String, String>();
		HashMap<String, String> objetoJson = hcService.obtenerCostoProducto(trace, codigoServicio, nroCupon);
		if(objetoJson.get("estado").equals("0")){
			if(objetoJson.containsKey("response_code") && objetoJson.get("response_code").equals("00")){
				data.put("estado", "0");
				data.put("descripcion", "ok");
				data.put("fechaIngreso", objetoJson.get("fecha"));
				data.put("fechaSalida", objetoJson.get("fechaSalida"));
				data.put("nroTicket", objetoJson.get("numero_ticket"));
				//TEMP_NRO_TICKET = objetoJson.getString("numero_ticket");
				data.put("codigoEntrada", objetoJson.get("estacionEntrada"));
				data.put("monto", objetoJson.get("monto"));
				data.put("impuesto", objetoJson.get("impuesto"));
				data.put("montoTotal", objetoJson.get("monto_total"));
				int tiempo=UtilWeb.minutosDiferencia(objetoJson.get("fecha"), objetoJson.get("fechaSalida"));
				data.put("tiempo", String.valueOf(tiempo));
				
				if(nroCupon.length()==12){
					data.put("payPassBalance", objetoJson.get("payPassBalance"));
					data.put("payPassConsumo", objetoJson.get("payPassConsumo"));
					data.put("payPassAmountOld", objetoJson.get("payPassAmountOld"));
					data.put("payPassCard", objetoJson.get("payPassCard"));
					data.put("payPassName", objetoJson.get("payPassName"));
					data.put("payPassDni", objetoJson.get("payPassDni"));
					data.put("payPassTrace", objetoJson.get("payPassTrace"));
				}
			}else if(objetoJson.containsKey("errorCode")){
				data.put("estado", objetoJson.get("errorCode"));
				data.put("descripcion", objetoJson.get("messageHost"));
			}else{
				data.put("estado", "2");
				data.put("descripcion", "Problemas con hipercenter");
			}
		}else{
			data.put("estado", "1");
			data.put("descripcion", "Error al conectarse al servicio de Hipercenter");
		}
		
		return data;
	}

}

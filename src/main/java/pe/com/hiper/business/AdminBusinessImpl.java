package pe.com.hiper.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.UtilMoneda;
import pe.com.hiper.entity.TmDevice;
import pe.com.hiper.model.Moneda;
import pe.com.hiper.service.DeviceService;
import pe.com.hiper.service.EvaCoreService;
import pe.com.hiper.service.HCService;

@Component("adminBusinessFujitsu")
public class AdminBusinessImpl implements AdminBusiness {

	@Autowired
	DeviceService deviceService;

	@Autowired
	HCService hcService;

	@Autowired
	EvaCoreService evaCoreService;

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Value("${kiosco.moneda}")
	private String monedaPais;
	
	@Value("${dispositivo.fujitsu.cod.error}")
	private String codErrorFujitsu;

	public HashMap<String, Object> dataAbastecimiento(String trace, boolean isManipulated) {
		HashMap<String, Object> mapHCenter = new HashMap<String, Object>();
		try {
			if (isManipulated)
				evaCoreService.invocarServicioGenerico(trace, Constantes.EVACORE_BILLETERO_UPDATE);

			mapHCenter.put("estado", "0");
			mapHCenter.put("descripcion", "Ok");

			List<HashMap<String, String>> dataHopper = arrayDataDispositivo("HO01");
			mapHCenter.put("hopper", dataHopper);// new Gson().fromJson(json.get("hopper").toString() , new
													// TypeToken<HashMap<String, String>[]>() {}.getType()) );

			List<HashMap<String, String>> dataSmartHopper = arrayDataDispositivo("SH01");
			mapHCenter.put("smartHopper", dataSmartHopper);// new Gson().fromJson(json.get("smartHopper").toString() ,
															// new TypeToken<HashMap<String, String>[]>() {}.getType())
															// );

//			mapHCenter.put( "reservorio", mapAbastecimiento("") );

			List<HashMap<String, String>> dataPayout = arrayDataDispositivo("PY01");
			mapHCenter.put("payout", dataPayout);// new Gson().fromJson(json.get("payout").toString() , new
													// TypeToken<HashMap<String, String>[]>() {}.getType()) );

			List<HashMap<String, String>> dataCbb = arrayDataDispositivo("CB01");
			mapHCenter.put("cashBox", dataCbb);
		} catch (Exception e) {
			mapHCenter.put("estado", "-1");
			mapHCenter.put("descripcion", "Error general");
			e.printStackTrace();
		}

		return mapHCenter;
	}

	public HashMap<String, String> abastecimiento(String trama, String trace, String nombreRol, String numTarjetaAdmin,
			String usuario){

		HashMap<String, String> data = new HashMap<String, String>();

		try {

			String tramaHP = "", tramaSM = "", tramaRV = "", tramaBI = "", tramaCBBI = "";

			String[] dispositivos = trama.split("\\#");
			for (int i = 0; i < dispositivos.length; i = i + 2) {
				String tramaTmp = dispositivos[i + 1];
				switch (dispositivos[i]) {
				case "HOPP":
					tramaHP = tramaTmp.substring(0, tramaTmp.length() - 1);
					break;
				case "SMHP":
					tramaSM = tramaTmp.substring(0, tramaTmp.length() - 1);
					break;
				case "RSEV":
					tramaRV = tramaTmp.substring(0, tramaTmp.length() - 1);
					break;
				case "BILL":
					tramaBI = tramaTmp.substring(0, tramaTmp.length() - 1);
					break;
				case "CBBI":
					tramaCBBI = tramaTmp.substring(0, tramaTmp.length() - 1);
					break;
				}
			}

			boolean actualizarMonedas = true;
			if (!tramaSM.equals("")) {
				String[] moneda = tramaSM.split(":");

				for (int i = 0; i < moneda.length; i++) {
					String[] datos = moneda[i].split(";");
					String estadoAgrCero = "0";// this.agregarMonedas(trace, 0,
												// formatoDenominacion(datos[0])).get("estado");

					String estadoAgrCant = "0";
					if (Integer.parseInt(datos[1]) != 0) {
						// estadoAgrCant = this.agregarMonedas(trace, Integer.parseInt(datos[1]),
						// formatoDenominacion(datos[0])).get("estado");
						estadoAgrCant = evaCoreService
								.agregarMonedasSH(trace, Integer.parseInt(datos[1]), formatoDenominacion(datos[0]))
								.get("estado");
					}

					if (!(estadoAgrCero + estadoAgrCant).equals("00")) {
						actualizarMonedas = false;
						break;
					}
				}
			}

			if (actualizarMonedas) {
				// JSONObject json = this.getHiperCenterService().recargarHopper(tramaHP,
				// tramaSM, tramaBI, tramaCBBI, "", "-", "2", "00", "00", trace, terminal, new
				// Constantes().RECARGA_FUJITSU, "01", nombreRol, numTarjetaAdmin, usuario);
				HashMap<String, String> json = hcService.recargarHopper(tramaHP, tramaSM, tramaBI, tramaCBBI, "", "-",
						"2", "00", "00", trace, nroTerminal, 2, "01", nombreRol, numTarjetaAdmin, usuario);
				if (json.get("estado").equals("0") || json.get("estado").equals("91")) {
					if (json.containsKey("print_data")) {
						data.put("estado", "0");
						data.put("descripcion", "Ok");
						data.put("textoImprimir", json.get("print_data"));
					} else {
						data.put("estado", "2");
						data.put("descripcion", "Error respuesta hipercenter");
					}
				} else {

					data.put("estado", "1");
					data.put("descripcion", "No se estableció conexión con hipercenter");
					data.put("textoImprimir", "");
				}
			} else {
				data.put("estado", "3");
				data.put("descripcion", "No se pudo registrar cantidades en dispositivo");
				data.put("textoImprimir", "");
			}

		} catch (Exception e) {

			data.put("estado", "-1");
			data.put("descripcion", "Error general");
			data.put("textoImprimir", "");
		}
		return data;
	}

	public HashMap<String, String> vaciarSmartHopper(String trace, String nombreRol, String numTarjetaAdmin,
			String usuario) {
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			HashMap<String, String> rpta = evaCoreService.invocarServicioGenerico(trace,
					Constantes.EVACORE_SMARTH_VACIAR);
			if (rpta.get("estado").equals("0")) {

				String tramaCero = UtilMoneda.getFormatMonedas(new HashMap<Double, Integer>(),
						deviceService.obtenerDispositivos("SH01"));

				System.out.println("TRAMA FORMATEADA DE MONEDAS " + tramaCero);

				List<HashMap<String, String>> dataSmartHopper = arrayDataDispositivo("SH01");
				List<HashMap<String, String>> dataCashBoxMonedas = arrayDataDispositivo("CB02");

				HashMap<String, String> jsonRecarga = hcService.recargarHopper("", tramaCero, "", "", "", "-", "2",
						"00", "00", trace, nroTerminal, 2, "02", nombreRol, numTarjetaAdmin, usuario);
				if (jsonRecarga.containsKey("estado") && jsonRecarga.get("estado").equals("0")) {
					if (jsonRecarga.containsKey("print_data")) {
						data.put("estado", "0");
						data.put("descripcion", "Ok");
						data.put("moneCashBox", tramaCashBox(dataSmartHopper, dataCashBoxMonedas));
						data.put("textoImprimir", jsonRecarga.get("print_data"));
					} else {
						data.put("estado", "2");
						data.put("descripcion", "Error respuesta hcenter");
						data.put("textoImprimir", "");
					}
				} else {
					data.put("estado", "1");
					data.put("descripcion", "No se estableció conexión con hipercenter");
				}
			} else {
				data.put("estado", rpta.get("estado"));
				data.put("descripcion", rpta.get("descripcion"));
			}
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error general");
		}

		return data;
	}

	public HashMap<String, String> vaciarCashBoxMonedas(String trace, String nombreRol, String numTarjetaAdmin,
			String usuario) {
		HashMap<String, String> data = new HashMap<String, String>();
		try {

			// String tramaCero = getTmDeviceDAO().consultarCantidadDispositivo(getConn(),
			// terminal, "CB02", true);
			String tramaCero = UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(),
					deviceService.obtenerDispositivos("CB02"));
			HashMap<String, String> jsonRecarga = hcService.recargarHopper("", "", "", "", tramaCero, "-", "2", "00",
					"00", trace, nroTerminal, 2, "05", nombreRol, numTarjetaAdmin, usuario);
			if (jsonRecarga.containsKey("estado") && jsonRecarga.get("estado").equals("0")) {
				if (jsonRecarga.containsKey("print_data")) {
					data.put("estado", "0");
					data.put("descripcion", "Ok");
					data.put("textoImprimir", jsonRecarga.get("print_data"));
				} else {
					data.put("estado", "2");
					data.put("descripcion", "Error respuesta hcenter");
					data.put("textoImprimir", "");
				}
			} else {
				data.put("estado", "1");
				data.put("descripcion", "No se estableció conexión con hipercenter");
			}
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error general");
		}

		return data;
	}

	public HashMap<String, String> iniciarPayoutAStacker(String trace, boolean manipulated, String nombreRol,
			String numTarjetaAdmin, String usuario) {
		HashMap<String, String> data = new HashMap<String, String>();

		List<HashMap<String, String>> dataStackers = arrayDataDispositivo("PY01");
		List<HashMap<String, String>> dataCashBox = arrayDataDispositivo("CB01");

		Pair<Boolean, Integer> espacioVaciar = validarEspacioCashBox(dataCashBox, dataStackers);

		if (espacioVaciar.getSecond() == 0) {
			data.put("estado", "3");
			data.put("descripcion", "No hay billetes en el dispositivo");
			return data;
		}

		if (espacioVaciar.getFirst()) {
			if (manipulated)
				evaCoreService.invocarServicioGenerico(trace, Constantes.EVACORE_BILLETERO_UPDATE);

			// HashMap<String, String> valVaciarPayout =
			// this.getDispositivoService().vaciarPayout(trace);
			HashMap<String, String> valVaciarPayout = evaCoreService.invocarServicioGenerico(trace,
					Constantes.EVACORE_BILLETERO_VACIAR);
			// LOGGER.info("RESULTADO DE PURGA DE DISPOSITIVO "+valVaciarPayout);

			if (valVaciarPayout.get("estado").equals("29711")) {
				// LOGGER.info(trace+" "+":::::: CASHIN ACTIVE ::::::");
				// this.getDispositivoService().solucionCashInActive(trace);
				evaCoreService.invocarServicioGenerico(trace, Constantes.EVACORE_BILLETERO_SOLUCION_CASH);
				// this.getDispositivoService().updateFujitsu();
				evaCoreService.invocarServicioGenerico(trace, Constantes.EVACORE_BILLETERO_UPDATE);
				valVaciarPayout = evaCoreService.invocarServicioGenerico(trace, Constantes.EVACORE_BILLETERO_VACIAR);
			}

			if (valVaciarPayout.get("estado").equals("0")) {
				// String tramaCero = getTmDeviceDAO().consultarCantidadDispositivo(getConn(),
				// terminal, "PY01", true);
				String tramaCero = UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(),
						deviceService.obtenerDispositivos("PY01"));
				String tramaCashBox = tramaCashBox(dataStackers, dataCashBox);

				HashMap<String, String> jsonDescarga = hcService.recargarHopper("", "", tramaCero, "", "", "-", "2",
						"00", "00", trace, nroTerminal, 2, "03", nombreRol, numTarjetaAdmin, usuario);

				if (jsonDescarga.containsKey("estado") && jsonDescarga.get("estado").equals("0")) {
					if (jsonDescarga.containsKey("print_data")) {
						data.put("estado", "0");
						data.put("descripcion", "Ok");
						data.put("billCashBox", tramaCashBox);
						data.put("textoImprimir", jsonDescarga.get("print_data"));
					} else {
						data.put("estado", "2");
						data.put("descripcion", "Error respuesta hcenter");
						data.put("billCashBox", "");
						data.put("textoImprimir", "");
					}
				} else {
					data.put("estado", "1");
					data.put("descripcion", "No se estableció conexión con hipercenter");
				}
			} else {
				data.put("estado", valVaciarPayout.get("estado"));
				data.put("descripcion", valVaciarPayout.get("descripcion"));
			}
		} else {
			data.put("estado", "4");
			data.put("descripcion", "Espacio insuficiente para vaciar Stackers");
		}
//				data.put("cantidadBilletes", String.valueOf(cantidadBilletes));
//			}

		return data;
	}

	public HashMap<String, String> vaciarCashBox(String trace, String nombreRol, String numTarjetaAdmin,
			String usuario) {
		HashMap<String, String> data = new HashMap<String, String>();
		try {

			String tramaCero = UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(),
					deviceService.obtenerDispositivos("CB01"));

			HashMap<String, String> jsonRecarga = hcService.recargarHopper("", "", "", tramaCero, "", "-", "2", "00",
					"00", trace, nroTerminal, 2, "04", nombreRol, numTarjetaAdmin, usuario);
			if (jsonRecarga.containsKey("estado") && jsonRecarga.get("estado").equals("0")) {
				if (jsonRecarga.containsKey("print_data")) {
					data.put("estado", "0");
					data.put("descripcion", "Ok");
					data.put("textoImprimir", jsonRecarga.get("print_data"));
				} else {
					data.put("estado", "2");
					data.put("descripcion", "Error respuesta hcenter");
					data.put("textoImprimir", "");
				}
			} else {
				data.put("estado", "1");
				data.put("descripcion", "No se estableció conexión con hipercenter");
			}
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error general");
		}

		return data;
	}

	public HashMap<String, String> getMontoXDispositivo(String trace, String dispositivo) {

		HashMap<String, String> data = new HashMap<String, String>();
		List<TmDevice> devices = deviceService.obtenerDispositivos(dispositivo);
		int cantidadMonedas = devices.stream().mapToInt(d -> Integer.parseInt(d.getnDsQuantityDenomination())).sum();
		data.put("estado", "0");
		data.put("descripcion", "Total de monedas");
		data.put("monedas", String.valueOf(cantidadMonedas));

		return data;
	}

	public HashMap<String, String> vaciarHopper(String trace, String nombreRol, String nroMonedas, String usuario) {

		HashMap<String, String> data = new HashMap<String, String>();
		try {

			HashMap<String, String> rpta = evaCoreService.dispensarHopper(trace, "0", Integer.parseInt(nroMonedas));
			int monNodDevueltos = Integer
					.parseInt(rpta.get("descripcion").toString().substring(0, 2).trim().replace(" ", ""));
			if (monNodDevueltos >= 0) {
				String tramaCero = UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(),
						deviceService.obtenerDispositivos("HO01"));

				List<HashMap<String, String>> dataHopper = arrayDataDispositivo("HO01");
				// ArrayList<HashMap<String, String>> dataCashBoxMonedas =
				// arrayDataDispositivo(terminal, "CB02");

				HashMap<String, String> jsonRecarga = hcService.recargarHopper(tramaCero, "", "", "", "", "-", "2",
						"00", "00", trace, nroTerminal, 2, "05", nombreRol, usuario, usuario);
				if (jsonRecarga.containsKey("estado") && jsonRecarga.get("estado").equals("0")) {
					if (jsonRecarga.containsKey("print_data")) {
						data.put("estado", "0");
						data.put("descripcion", "Ok");
						data.put("moneHopper", tramaCashBox(new ArrayList<HashMap<String, String>>(), dataHopper));
						data.put("textoImprimir", jsonRecarga.get("print_data"));
					} else {
						data.put("estado", "2");
						data.put("descripcion", "Error respuesta hcenter");
						data.put("textoImprimir", "");
					}
				} else {
					data.put("estado", "1");
					data.put("descripcion", "No se estableció conexión con hipercenter");
				}
				data.put("noDevuelto", String.valueOf(monNodDevueltos));
			} else {
				data.put("estado", "-1");
				data.put("descripcion", rpta.get("estado"));
			}
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error general");

		}
		return data;
	}

	public HashMap<String, String> generarReporte(String trace, String tipoTrx, String tipoTrxBus, int numTrxBus,
			String nombreRol, String numTrjUsuario) {

		HashMap<String, String> data = new HashMap<String, String>();

		try {

			HashMap<String, String> json = new HashMap<String, String>();

			switch (tipoTrx) {
			case "1":
				json = hcService.estadoHopSMHopBill(trace, "2", "00", "00", nroTerminal, nombreRol, numTrjUsuario);
				break;
			case "2":
				json = hcService.reportePortales("REPORTE_DIARIO", tipoTrxBus, numTrxBus, trace, nroTerminal, nombreRol,
						numTrjUsuario);
				break;
			case "3":
				json = hcService.reportePortales("REPORTE_NOTACREDITO", "", numTrxBus, trace, nroTerminal, nombreRol,
						numTrjUsuario);
				break;
			case "4":
				json = hcService.reportePortales("REPORTE_ERRORES_TRX", "", numTrxBus, trace, nroTerminal, nombreRol,
						numTrjUsuario);
				break;
			case "5":
				json = hcService.reportePortales("REPORTE_CUADRE_OPER", "", numTrxBus, trace, nroTerminal, nombreRol,
						numTrjUsuario);
				break;
			case "6":
				json = hcService.reporteUlt5TrxPortales(trace, nroTerminal, nombreRol, numTrjUsuario);
				break;
			case "7":
				json = hcService.estadoHopSMHopBill(trace, "6", "00", "00", nroTerminal, nombreRol, numTrjUsuario);
				break;
			}

			if (json.containsKey("estado") && json.get("estado").equals("0")) {
				if (json.containsKey("print_data")) {
					data.put("estado", "0");
					data.put("descripcion", "OK");
					data.put("textoImprimir", json.get("print_data"));
				} else {

					data.put("estado", "2");
					data.put("descripcion", "Error respuesta hcenter");
					data.put("textoImprimir", "");
				}
			} else {
				data.put("estado", "1");
				data.put("descripcion", "No se estableció conexión con hcenter");
				data.put("textoImprimir", "");
			}
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error general");
			data.put("textoImprimir", "");
		}
		return data;
	}
	
	public HashMap<String, Object> ingresarBillAbastecimiento(String trace){
		HashMap<String, Object> mapBilletes = new HashMap<String, Object>();
		
		try{
//			HashMap<String, Integer> antes = dispositivoService.getBilletesPorComponente();
			Moneda billetes=evaCoreService.aceptarBilletes(trace);
			//Moneda billetes = dispositivoService.aceptarBilletes(trace);
			if(billetes.getEstado()==20010){ 
				//LOGGER.info(trace+" "+"Controlador ocupado");
				//HashMap<String, String> rptaConectar = dispositivoService.conectarFujitsu(trace);
				HashMap<String, String> rptaConectar= evaCoreService.invocarServicioGenerico(trace, Constantes.EVACORE_BILLETERO_CANCELAR);
				if(rptaConectar.get("estado").equals("0")){
					billetes = evaCoreService.aceptarBilletes(trace);
				}
			} else if( errorRecalcular(billetes.getEstado()) ){ //No cashin active
				//resetFujitsu(trace, 1);
				return ingresarBillAbastecimiento(trace);
			}
			
			//dispositivoService.finalizarScrow(trace, false); // ???
			evaCoreService.finalizarScrow(trace, false);
			
			if(billetes.getEstado()==0){
				// Obtener cantidad de billetes finales por denominacion
				HashMap<String, Integer> despues = evaCoreService.getBilletesPorComponente(trace);
				
				//LOGGER.info(trace+" getBilletesPorComponente "+despues.toString());
				//String terminal = readProperty.getString("terminal");
				//String tramaDispositivo = getTmDeviceDAO().consultarCantidadDispositivo(getConn(), terminal, "PY01", true);
				String tramaDispositivo = UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(), deviceService.obtenerDispositivos("PY01"));
				String[] denom = tramaDispositivo.split(":");

				ArrayList<HashMap<String, Integer>> listaAumento = new ArrayList<HashMap<String,Integer>>();
				
				for (int i = 0; i < denom.length; i++) {
					String[] datos = denom[i].split(";");
					Moneda BilleteActual=null;
					for (Moneda temp : billetes.getBilletes()) {
						//LOGGER.info(trace+" "+"temp.getDenominacion() -> "+temp.getDenominacion()+"\t == "+(Integer.parseInt(datos[0])/100));
						if(temp.getDenominacion()==(Integer.parseInt(datos[0])/100)){
							BilleteActual= temp;
							break;
						}
					}
					HashMap<String, Integer> aumentoXdenom = new HashMap<String, Integer>();
//					aumentoXdenom.put( "posicion", Integer.parseInt(datos[2].substring(datos[2].length()-1, datos[2].length())) );
					aumentoXdenom.put( "denominacion", Integer.parseInt(datos[0]) );
					aumentoXdenom.put( "cantidad", (Integer)despues.get(datos[0]));
					listaAumento.add(aumentoXdenom);
				}
				
				//String tramaCashbox = getTmDeviceDAO().consultarCantidadDispositivo(getConn(), terminal, "CB01", true);
				String tramaCashbox = UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(), deviceService.obtenerDispositivos("CB01"));
				for(Moneda temp : billetes.getBilletes()){
					if(temp.getCantidadCashbox()>0){
						String denomBillCB = String.valueOf(temp.getDenominacion()*100);
						tramaCashbox=tramaCashbox.replace(denomBillCB+";0", denomBillCB+";"+temp.getCantidadCashbox());
						//LOGGER.info(trace+" "+"denomBillCB -> "+denomBillCB+"\t "+tramaCashbox);
					}
				}
				mapBilletes.put("cashBox", tramaCashbox);
				
				

				
				mapBilletes.put("estado", billetes.getEstado());
				mapBilletes.put("descripcion", billetes.getDescripcion());
//				mapBilletes.put("billetes", billetes.getBilletes());
				mapBilletes.put("billetes", listaAumento);
			} else {
				mapBilletes.put("estado", billetes.getEstado());
				mapBilletes.put("descripcion", billetes.getDescripcion());
				mapBilletes.put("billetes", null);
			}
		}catch(Exception e){
			mapBilletes.put("estado", "-1");
			mapBilletes.put("descripcion", "Error general");		
		}
		
		//LOGGER.info(trace+" "+"MAPBILLETES  "+mapBilletes);
		return mapBilletes;
	}

	public HashMap<String, String> tramaSMH() {

		HashMap<String, String> data = new HashMap<String, String>();
	
		String tramaSmartHopper = UtilMoneda.getFormatData(deviceService.obtenerDispositivos("SH01"));
		
		data.put("tramaSmartHopper", tramaSmartHopper);
	
		return data;
	
	}
	
	private boolean errorRecalcular(int codError){
		try{
			//LOGGER.info("Verificando si es un error CASHIN :: "+codError);
			
			String[] codPosibles = codErrorFujitsu.split(",");
			
			if(codError!=0){
				for (int i = 0; i < codPosibles.length; i++) {
					if(codError == Integer.parseInt(codPosibles[i]) )
						return true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return false;
	}

	private Pair<Boolean, Integer> validarEspacioCashBox(List<HashMap<String, String>> mapCashBox,
			List<HashMap<String, String>> mapPayout) {
		Pair<Boolean, Integer> data = null;
		int cantReciclador = 0, cantPayout = 0;

		for (int j = 0; j < mapPayout.size(); j++) {
			cantPayout += Integer.parseInt(mapPayout.get(j).get("cantidad"));
		}
		for (int i = 0; i < mapCashBox.size(); i++) {
			cantReciclador += Integer.parseInt(mapCashBox.get(i).get("cantidad"));
		}

		if ((cantReciclador + cantPayout) <= 1500) {
			// return new Pair<Boolean, Integer>(true, cantPayout);
			return data.of(true, cantPayout);
		} else {
			return data.of(false, cantPayout);
		}
	}

	private String tramaCashBox(List<HashMap<String, String>> mapPayout, List<HashMap<String, String>> mapCashBox) {
		for (int i = 0; i < mapPayout.size(); i++) {
			for (int j = 0; j < mapCashBox.size(); j++) {
				if (mapCashBox.get(j).get("denominacion").equals(mapPayout.get(i).get("denominacion"))) {
					int suma = Integer.parseInt(mapCashBox.get(j).get("cantidad"))
							+ Integer.parseInt(mapPayout.get(i).get("cantidad"));
					mapCashBox.get(j).put("cantidad", String.valueOf(suma)); // mapPayout.get(i).get("cantidad"));
				}
			}
		}

		return tramaDispositivo(mapCashBox, false);
	}

	private String tramaDispositivo(List<HashMap<String, String>> data, boolean cantCero) {
		String tramaCero = "";
		for (int i = 0; i < data.size(); i++) {
			String denoSinPunto = data.get(i).get("denominacion").replace(".", "");
			String denoSinCerosIzq = denoSinPunto.replaceFirst("^0*", "");

			if (cantCero)
				tramaCero += denoSinCerosIzq + ";" + 0 + ";" + data.get(i).get("codeMoney") + ":";
			else
				tramaCero += denoSinCerosIzq + ";" + data.get(i).get("cantidad") + ";" + data.get(i).get("codeMoney")
						+ ":";
		}
		return tramaCero.substring(0, tramaCero.length() - 1);
	}

	private List<HashMap<String, String>> arrayDataDispositivo(String deviceCode) {
		ArrayList<HashMap<String, String>> data = new ArrayList<>();

		List<TmDevice> lista = deviceService.obtenerDispositivos(deviceCode);
		for (TmDevice tmDevice : lista) {
			HashMap<String, String> temp = new HashMap<>();
			temp.put("denominacion", tmDevice.getcDsCodeDenomination());
			temp.put("cantidad", tmDevice.getnDsQuantityDenomination());
			temp.put("codeMoney", tmDevice.getcDsMoneyCode());

			data.add(temp);
		}

		return data;
	}

	private double formatoDenominacion(String denominacion) {
		Double denoInt = Double.parseDouble(denominacion) / 100;

		if (monedaPais.equals("PEN")) {
			return denoInt;
		} else if (monedaPais.equals("COP")) {
			denoInt = denoInt / 100;
			return denoInt;
		} else {
			return denoInt;
		}
	}

	@Override
	public HashMap<String, String> desactivarDispositivos(String trace, String tipo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<String, Object> consultarEfectivoIngresado(String trace) {
		// TODO Auto-generated method stub
		return null;
	}

	

}

package pe.com.hiper.business;

import java.util.HashMap;


public interface AdminBusiness {

	public HashMap<String, Object> dataAbastecimiento(String trace, boolean isManipulated);

	public HashMap<String, String> abastecimiento(String trama, String trace, String nombreRol, String numTarjetaAdmin,
			String usuario);

	public HashMap<String, String> vaciarSmartHopper(String trace, String nombreRol, String numTarjetaAdmin,
			String usuario);

	public HashMap<String, String> vaciarCashBoxMonedas(String trace, String nombreRol, String numTarjetaAdmin,
			String usuario);

	public HashMap<String, String> iniciarPayoutAStacker(String trace, boolean manipulated, String nombreRol,
			String numTarjetaAdmin, String usuario);

	public HashMap<String, String> vaciarCashBox(String trace, String nombreRol, String numTarjetaAdmin,
			String usuario);

	public HashMap<String, String> getMontoXDispositivo(String trace, String dispositivo);

	public HashMap<String, String> vaciarHopper(String trace, String nombreRol, String nroMonedas, String usuario);

	public HashMap<String, String> generarReporte(String trace, String tipoTrx, String tipoTrxBus, int numTrxBus,
			String nombreRol, String numTrjUsuario);
	
	public HashMap<String, Object> ingresarBillAbastecimiento(String trace);
	
	public HashMap<String, String> desactivarDispositivos(String trace, String tipo);
	
	public HashMap<String, Object> consultarEfectivoIngresado(String trace);

	public HashMap<String, String> tramaSMH();

}

package pe.com.hiper.business;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import pe.com.hiper.common.CalcularVueltoBilletes;
import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.UtilMoneda;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.CBDevice;
import pe.com.hiper.entity.TmDevice;
import pe.com.hiper.entity.TpMontoPagado;
import pe.com.hiper.entity.TpMontoPagar;
import pe.com.hiper.model.Moneda;
import pe.com.hiper.service.DeviceService;
import pe.com.hiper.service.EvaCoreService;
import pe.com.hiper.service.HCService;
import pe.com.hiper.service.MontoPagadoService;
import pe.com.hiper.service.MontoPagarService;
import pe.com.hiper.service.TransactionLogService;

@Component
public class PagoBussines {

	private static final Logger LOGGER = LogManager.getLogger(PagoBussines.class);
	@Autowired
	MontoPagadoService montoPagadoService;

	@Autowired
	MontoPagarService montoPagarService;

	@Autowired
	EvaCoreService evaCoreService;

	@Autowired
	DeviceService deviceService;

	@Autowired
	HCService hcService;
	
	@Autowired
	TransactionLogService transactionService;

	@Autowired
	private ModelMapper modelMapper;

	@Value("${monedero.monedas.max}")
	private int cantidadMaximaMonedas;

	@Value("${kiosco.moneda}")
	private String monedaPais;

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Value("${hopper.monedas.max}")
	private int cantidadMaximaSH;

	@Value("${hcenter.agencia}")
	private String agencia;

	private static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	private TimerTask taskMonederoPolling;
	private ScheduledFuture<?> resultTareaMonedero = null;

	private final int BILLETES = 1;
	private final int MONEDAS = 2;

	private final int BILLETES_CASHBOX = 0;
	private final int BILLETES_STACKER = 1;

	public HashMap<String, String> habilitarIngresoEfectivo(String id_producto, String id_cliente, double costoServicio,
			int timeOut, String trace, int recarga, int flagInicio) {
		LOGGER.info(trace + " INICIO habilitarIngresoEfectivo");
		HashMap<String, String> dato = new HashMap<String, String>();
		TpMontoPagar montoPagar = montoPagarService.getMontoPagar(trace);

		if (montoPagar == null) {
			UtilWeb.MONTO_INGRESADO = 0;
			montoPagar = new TpMontoPagar();
			montoPagar.setnCostoServicio(costoServicio);
			montoPagar.setdFechaIngreso(new Date());
			montoPagar.setcCodCliente(id_cliente);
			montoPagar.setnCodProducto(id_producto);
			montoPagar.setcTrace(trace);
			montoPagarService.saveMontoPagar(montoPagar);
		} else {
			// System.out.println("TpMontoPagar -> " + montoPagar.toString());
			List<TpMontoPagado> listaMontoPagados = montoPagadoService.listarMontoPagado(trace);
			UtilWeb.MONTO_INGRESADO = (listaMontoPagados.stream().mapToDouble(monto -> monto.getnDenominacion()).sum()
					+ UtilWeb.MONTO_INGRESADO);
		}
		System.out.println("MONTO INGRESADO -> " + UtilWeb.MONTO_INGRESADO);
		dato.put("estado", "0");
		dato.put("descripcion", "ok");
		iniciarAceptadorMonedas(trace, costoServicio);
		return dato;
	}

	public HashMap<String, Object> habilitarIngresoDeBilletes(String trace) {
		Moneda moneda = new Moneda();
		HashMap<String, Object> data = new HashMap<String, Object>();
		TpMontoPagado montoPagado = null;
		try {
			moneda = evaCoreService.aceptarBilletes(trace);
			if (moneda.getEstado() == 0) {
				data.put("estado", "0");
				data.put("descripcion", "OK");
				List<Moneda> billetes = moneda.getBilletes();
				data.put("monto", UtilWeb.MONTO_INGRESADO);
				data.put("billetes", billetes);
				for (Moneda billetePay : billetes) {
					for (int j = 0; j < billetePay.getCantidadPayout(); j++) {
						montoPagado = new TpMontoPagado();
						montoPagado.setnTipo(BILLETES);
						montoPagado.setnDenominacion(Double.parseDouble(String.valueOf(billetePay.getDenominacion())));
						montoPagado.setcTrace(trace);
						montoPagado.setBpayout(1);
						montoPagadoService.saveMontoPagado(montoPagado);
					}
				}
				for (Moneda billeteCashBox : billetes) {

					for (int k = 0; k < billeteCashBox.getCantidadCashbox(); k++) {
						montoPagado.setnTipo(BILLETES);
						montoPagado
								.setnDenominacion(Double.parseDouble(String.valueOf(billeteCashBox.getDenominacion())));
						montoPagado.setcTrace(trace);
						montoPagado.setBpayout(0);
						montoPagadoService.saveMontoPagado(montoPagado);
					}
				}
//				
			} else {
				data.put("estado", "1");
				data.put("descripcion", moneda.getDescripcion());
				data.put("monto", 0.0);
				data.put("billetes", moneda.getBilletes());
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Finalizo");
		}

		return data;
	}

	public HashMap<String, String> detenerIngresoEfectivo(String trace) {
		HashMap<String, String> data = new HashMap<String, String>();
		detenerAceptadorMonedas();
		data.put("estado", "1");
		data.put("descripcion", "Servicio ok");

		return data;
	}

	public HashMap<String, Object> consultarEfectivoIngresado(String trace) {
		HashMap<String, Object> data = new HashMap<String, Object>();

		data.put("estado", "1");
		data.put("descripcion", "ok");
		data.put("monto", UtilWeb.MONTO_INGRESADO);
		return data;
	}
	
	public HashMap<String, String> cancelarUltimoComando(String trace) {
		HashMap<String, String> data = new HashMap<String, String>();

		data=evaCoreService.invocarServicioGenerico(trace, Constantes.EVACORE_BILLETERO_CANCELAR_UTL);
		return data;
	}

	public HashMap<String, Object> procesarPago(String trace, String tipoPago, String nombre_cliente, String dni,
			String telefono, String correo, String descProducto, String nro_documento, String flagTPP,
			String codigoVerificacion) {

		HashMap<String, Object> retValue = new HashMap<String, Object>();

		if (!UtilWeb.TRACE_PROCESADO.equals(trace)) {

			UtilWeb.TRACE_PROCESADO = trace;
			// flagCancelar=false;
			// registrarBilletes(trace); utilizado cuando es nv200
			String tipoAceptador = "1";// getReadProperty().getString("tipoAceptador");

			Double montoVuelto = 0.0;
			List<Moneda> vueltoBilletes = new ArrayList<Moneda>();
			// ingresoMonedas=false;

			String flagRecicla = "0";// = this.getDispositivoService().isReciclador(trace) ? "0" : "1";

			int denominador = 1;
			String monedaPais = this.monedaPais;
			if (monedaPais.equals("COP")) {
				denominador = 100;
			}

			try {
				double montoPagadoTotal = 0.0;

				// MontoPagar montoPagar =
				// this.getMontoPagarDAO().obtenerMontoPagarPorTrace(this.getConn(), trace);
				TpMontoPagar montoPagar = montoPagarService.getMontoPagar(trace);
				double costoServicio = montoPagar.getnCostoServicio();

				// List<MontoPagado> montoPagadoList =
				// this.getMontoPagadoDAO().obtenerMontoPagadoPorTrace(this.getConn(), trace);
				List<TpMontoPagado> montoPagadoList = montoPagadoService.listarMontoPagado(trace);
				String[] hoppers = new String[3];

				HashMap<String, Object> dataDenominacion = UtilMoneda.obtenerDenominacionIngresada(montoPagadoList);
				HashMap<Double, Integer> monedas = (HashMap<Double, Integer>) dataDenominacion
						.get(UtilMoneda.DENO_MONEDAS);
				HashMap<Integer, Integer> billetes = (HashMap<Integer, Integer>) dataDenominacion
						.get(UtilMoneda.DENO_BILLETES_CASHBOX);
				HashMap<Integer, Integer> billetesPayout = (HashMap<Integer, Integer>) dataDenominacion
						.get(UtilMoneda.DENO_BILLETES_PAYOUT);
				montoPagadoTotal = (double) dataDenominacion.get(UtilMoneda.TOTAL_INGRESADO);

				System.out.println("*******MONEDAS******");
				System.out.println(monedas);
				System.out.println("*******BILLETES******");
				System.out.println(billetes);
				System.out.println("*******BILLETES PAUOUT******");
				System.out.println(billetesPayout);

				// montoPagadoTotal = montoPagadoTotal + montoPagoParcial;

				// System.out.println(finalizaScrow);
				/* FIN INGRESAR LOS BILLETES AL SCROW */
				boolean flagVuelto = false;
				System.out.println("montoPagadoTotal -> " + montoPagadoTotal);
				System.out.println("getnCostoServicio -> " + montoPagar.getnCostoServicio());
				if (montoPagadoTotal >= montoPagar.getnCostoServicio()) {

					CalcularVueltoBilletes objetoVuelto = new CalcularVueltoBilletes();
					List<TmDevice> listaDispositivos = deviceService.obtenerDispositivos("PY01");
					List<TmDevice> resultado = objetoVuelto.calcularDenominacionBilletes(
							String.valueOf(montoPagar.getnCostoServicio()), String.valueOf(montoPagadoTotal),
							listaDispositivos);

					double saldoMonedas = 0.0;

					for (TmDevice device : resultado) {
						if (device.getcDsCodeDenomination().equals("saldoMonedas")) {
							saldoMonedas = Double.parseDouble(device.getnDsQuantityDenomination().replace(",", "."));
							if (saldoMonedas > 0) {
								flagVuelto = true;
							}
						} else if (!device.getcDsCodeDenomination().equals("vuelto")) {
							vueltoBilletes.add(new Moneda(Integer.parseInt(device.getcDsCodeDenomination()),
									Integer.parseInt(device.getnDsQuantityDenomination()), device.getcDsMoneyCode()));
							flagVuelto = true;
						}
					}

					System.out.println("DISPENSAR " + saldoMonedas + " EN MONEDAS");

					String camino = "1";
					if (!monedas.isEmpty()) {
						System.out.println("SE INGRESARON MONEDAS");
						camino = derivarMonedas(trace);

					}

					String formatBilletesCashbox = UtilMoneda.getFormatBilletes(billetes,
							deviceService.obtenerDispositivos("CB01"));
					System.out.println("formatBilletesCashbox -> " + formatBilletesCashbox);

					String formatBilletesPayout = UtilMoneda.getFormatBilletes(billetesPayout, listaDispositivos);
					System.out.println("formatBilletesPayout -> " + formatBilletesPayout);

					HashMap<String, String> resultPago = hcService.procesarPagoTotalNuevo(
							montoPagar.getnCostoServicio(), montoPagadoTotal, montoPagar.getnCodProducto(), dni,
							formatBilletesCashbox, "", trace, "", "", "", descProducto, agencia, nroTerminal, flagTPP,
							flagRecicla, formatBilletesPayout, camino, "", "", "", "");

					if (resultPago.get("response_code").equals("00")) {

						if (tipoAceptador.equals("4") && (!billetes.isEmpty() || !billetesPayout.isEmpty())) {
							// AGREGAR METODOS PARA FINALIZAR SCROW DEL BILLETERO
							evaCoreService.finalizarScrow(trace, false);
						}
						actualizarCantidadMonedas(trace, monedas);
						retValue.put("textoImprimir", resultPago.get("print_data"));
						retValue.put("textoImprimirVuelto", resultPago.get("print_data"));
						// this.getDispositivoService().agregarMonedas(trace, cantidad, denominacion);

						retValue.put("vuelto", flagVuelto ? "0" : "1");
						retValue.put("estado", "0");
						retValue.put("descripcion", "pago realizado con exito");
						/*System.out.println("NRO DE TRACE "+trace+"\t nroTerminal -> "+nroTerminal);
						transactionService.actualizarMonedasIngresada(trace, nroTerminal, UtilMoneda.getFormatMonedas(monedas, deviceService.obtenerDispositivos("PY01")));
						System.out.println("ACTUALIZAR MONEDAS OK ");*/

					} else {

						retValue.put("vuelto", "1");
						retValue.put("estado", "1");
						retValue.put("descripcion", "No se pudo procesar el pago");
					}

					/*
					 * JSONObject resultPagoTotal =
					 * this.getHiperCenterService().procesarPagoTotalNuevo(montoPagar.
					 * getCostoServicio(), montoPagadoTotal, montoPagar.getIdServicio(), dni,
					 * formatBilletesCashBox(billetes), "", trace, hoppers[0], hoppers[1],
					 * hoppers[2], descProducto, this.getReadProperty().getString("agencia"),
					 * this.getReadProperty().getString("terminal"), flagTPP, flagRecicla,
					 * formatBilletesPayout(billetesPayout), camino, "", "" , "", "");
					 * System.out.println("resultPagoTotal -> "+resultPagoTotal);
					 * if(resultPagoTotal.get("response_code").equals("00")){
					 * retValue.setTextoImprimir(resultPagoTotal.getString("print_data"));
					 * this.LOGGER.info(trace+" "+"costoServicio "+costoServicio);
					 * if(tipoAceptador.equals("4")){ if(costoServicio>0){
					 * this.LOGGER.info(trace+" "+"finalizarScrow  "+costoServicio);
					 * this.getDispositivoService().finalizarScrow(trace, false);
					 * 
					 * } }
					 * 
					 * 
					 * String monedasCadena[]=formatMonedasSH(monedas).split(":"); for(int i=0;
					 * i<monedasCadena.length; i++){ String
					 * denoMoneda[]=monedasCadena[i].split(";");
					 * actualizarCantidadDispositivo(terminal, Integer.parseInt(denoMoneda[1]),
					 * denoMoneda[0], deviceCode, denominador); }
					 * retValue.setTextoImprimir(resultPagoTotal.getString("print_data"));
					 * retValue.setTextoImprimirVuelto(resultPagoTotal.getString("print_data"));
					 * //this.getDispositivoService().agregarMonedas(trace, cantidad, denominacion);
					 * retValue.setVuelto(flagVuelto?"0":"1"); retValue.setEstado("0");
					 * retValue.setDescripcion("pago realizado con exito");
					 * getTransactionLogDAO().registrarVueltoPago(getConn(), trace, terminal,
					 * "nTxCardNumber2",formatMonedasSH(monedas)); } else{ retValue.setVuelto("1");
					 * retValue.setEstado("1");
					 * retValue.setDescripcion("No se pudo procesar el pago");
					 * 
					 * }
					 */
				}

				/*
				 * if(!flagVuelto){ boolean flagMontoPagado =
				 * this.getMontoPagadoDAO().eliminarMontoPagado(this.getConn()); boolean
				 * flagMontoPagar = this.getMontoPagarDAO().eliminarMontoPagar(this.getConn());
				 * 
				 * } componenteLog.sethTxServiceOutput(UtilFuncion.getHoraActual());
				 */

			} catch (Exception e) {
				/*
				 * componenteLog.sethTxServiceOutput(UtilFuncion.getHoraActual());
				 * componenteLog.setbTxError(false); LOGGER.error(trace+" "+"Estado: " +
				 * retValue.getEstado() + " Error: ", e);
				 */
				e.printStackTrace();
			} finally {
				// socketCliente.cerrarSession();
			}
		} else {
			/*
			 * componenteLog.sethTxServiceOutput(UtilFuncion.getHoraActual());
			 * retValue.setEstado("0");
			 * retValue.setDescripcion("eL NRO DE TRACE YA HA SIDO PROCESADO");
			 * this.LOGGER.info(trace+" EL NRO DE TRACE YA HA SIDO PROCESADO");
			 */
		}
		/*
		 * TpComponenteLogDAO componentDAO = new TpComponenteLogImplSQL();
		 * componentDAO.guardarComponente(componenteLog);
		 */

		return retValue;
	}

	private String derivarMonedas(String trace) {
		String camino = "1";
		List<TmDevice> monedasSH = deviceService.obtenerDispositivos("SH01");
		int cantidadActualMoneda = monedasSH.stream()
				.mapToInt(device -> Integer.parseInt(device.getnDsQuantityDenomination())).sum();
		System.out.println("cantidad de monedas actuales -> " + cantidadActualMoneda);
		// FALTA IMPLEMENTAR MEDOTO PARA DECIR DONDE SE GUARDAN LAS MONEDAS

		if (cantidadActualMoneda >= cantidadMaximaSH) {
			// implementar this.getDispositivoService().scrowDevolver(trace);
			evaCoreService.devolverScrow(trace);
			camino = "1";
		} else {
			// this.getDispositivoService().scrowAceptar(trace);
			evaCoreService.aceptarScrow(trace);
			camino = "0";
		}

		return camino;
	}

	private void actualizarCantidadMonedas(String trace, HashMap<Double, Integer> monedas) {
		System.out.println("actualizarCantidadMonedas -> " + monedas);
		for (Entry<Double, Integer> entry : monedas.entrySet()) {
			// System.out.println("Clave: " + entry.getKey() + ", valor: " +
			// entry.getValue());
			int denominacion = (int) (entry.getKey() * 100);
			evaCoreService.agregarMonedasSH(trace, entry.getValue(), entry.getKey());
			TmDevice tmdevice=deviceService.getDenominacionDispositivo(String.valueOf(denominacion), "SH01");
			tmdevice.setnDsQuantityDenomination((Integer.parseInt(tmdevice.getnDsQuantityDenomination())+entry.getValue())+"");
			//deviceService.actualizarCantidadDenominacion(entry.getValue(), String.valueOf(denominacion), "SH01");
			deviceService.actualizarCantidad(tmdevice);
		}
	}

	private void iniciarAceptadorMonedas(String trace, double costoServicio) {

		if (UtilWeb.TOTAL_MONEDAS_INGRESADAS < cantidadMaximaMonedas) {

			HashMap<String, String> dataIngreso = evaCoreService.serviciosMonedero(trace,
					Constantes.EVACORE_MONEDERO_INICIAR);
			System.out.println("dataIngreso");

			if (dataIngreso.get("estado").equals("1")) {
				taskMonederoPolling = new TaskMonederoPolling(trace, costoServicio);
				if (scheduler.isShutdown()) {
					scheduler = Executors.newScheduledThreadPool(1);
				}
				resultTareaMonedero = scheduler.scheduleAtFixedRate(this.taskMonederoPolling, 500, 500,
						TimeUnit.MILLISECONDS);
			}
		}
	}

	public void detenerAceptadorMonedas() {

		System.out.println("********INGRESO AL METODO DETENERACEPTADORMONEDAS*********");
		System.out.println("");
		if (!resultTareaMonedero.isCancelled()) {
			System.out.println("CANCELAR");
			resultTareaMonedero.cancel(true);
			if (!scheduler.isShutdown())
				System.out.println("DETENER");
			{
				scheduler.shutdown();
			}
		}

	}

	/* AQUIE EL POOLEO DE monedas */
	private class TaskMonederoPolling extends TimerTask {
		int contador = 0;
		String trace;
		double costo;

		TaskMonederoPolling(String trace, double costo) {
			this.trace = trace;
			this.costo = costo;
		}

		@Override
		public synchronized void run() {
			// TODO Auto-generated method stub
			try {
				HashMap<String, String> dataIngreso = evaCoreService.serviciosMonedero(trace,
						Constantes.EVACORE_MONEDERO_DENOMINACIO);
				System.out.println("contador -> " + contador++);
				System.out.println(dataIngreso);

				if (dataIngreso.get("estado").equals("1") && !dataIngreso.get("descripcion").equals("0.0")) {

					BigDecimal montoIngresado = new BigDecimal(
							UtilWeb.MONTO_INGRESADO + Double.parseDouble(dataIngreso.get("descripcion")));
					montoIngresado = montoIngresado.setScale(2, RoundingMode.HALF_UP);

					UtilWeb.MONTO_INGRESADO = montoIngresado.doubleValue();

					int denominacion = (int) Double.parseDouble(dataIngreso.get("descripcion"));
					TpMontoPagado montoPagado = new TpMontoPagado();
					montoPagado.setnDenominacion(denominacion);
					montoPagado.setcTrace(trace);
					montoPagado.setnTipo(2);
					montoPagadoService.saveMontoPagado(montoPagado);

				}

				if (UtilWeb.MONTO_INGRESADO >= costo) {
					detenerAceptadorMonedas();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}
}

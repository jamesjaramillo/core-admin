package pe.com.hiper.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.model.Moneda;

@Component("adminBusinessNv200")
public class AdminBusinessNv200Impl extends AdminBusinessImpl {
	
	private static final Logger LOGGER = LogManager.getLogger(AdminBusinessNv200Impl.class);
	public double MONTO_BILLETES;
	public static double TEMP_MONTO_BILLETES=0;
	
	public static int ingresandoBillete=0;
	public HashMap<String, Object> ingresarBillAbastecimiento(String trace){
	
		HashMap<String, Object> mapBilletes = new HashMap<String, Object>();
		
		iniciarAceptadorBilletes(trace);
		mapBilletes.put("estado", "0");
		mapBilletes.put("descripcion", "Billetero habilitado");
		return mapBilletes;
	}
	
	public HashMap<String, String> desactivarDispositivos(String trace, String tipo) {
		HashMap<String, String> data=new HashMap<String, String>();
		String dato="0";
		if(tipo.equals("0")){
			//detenerAceptadorMonedas();
		}
		else if(tipo.equals("1")){
			detenerAceptadorbilletes(trace);
		}
		data.put("estado", "1");
		data.put("descripcion", "Dispositivo desactivado");
		
		return data;
	}
	
	public HashMap<String, Object> consultarEfectivoIngresado(String trace) {		
		
		HashMap<String, Object> dataBilletes=obtenerBilletesIngresados(trace);
		if(dataBilletes.containsKey("billetes")){
			dataBilletes.put("billetes", dataBilletes.get("billetes"));
		}
		
		dataBilletes.put("estado", "1");
		dataBilletes.put("descripcion", "ok");
		dataBilletes.put("monto", UtilWeb.MONTO_INGRESADO);
		dataBilletes.put("estadoDispositivo", String.valueOf(ingresandoBillete));	
		
		return dataBilletes;
	}
	
	private void detenerAceptadorbilletes(String trace){
		try {
			//this.getDispositivoService().cancelarScrow(trace);
			evaCoreService.invocarServicioGenerico(trace, Constantes.EVACORE_BILLETERO_CANCELAR);
			Thread.sleep(500);
			//this.getDispositivoService().finalizarScrow(trace, false);
			evaCoreService.finalizarScrow(trace, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void iniciarAceptadorBilletes(String trace) {
		evaCoreService.aceptarBilletes(trace);
	}
	
	private HashMap<String, Object> obtenerBilletesIngresados(String trace){
		this.LOGGER.info(trace+" "+"Executing metodo obtenerBilletesIngresados()");
		HashMap<String, Object> data=new HashMap<String, Object>();
		//Moneda data=new Moneda();
		try {
			List<Moneda> dataBilletes=new ArrayList<Moneda>();
			//Moneda billetesIngresados=this.getDispositivoService().billeteroConsultarIngreso(trace);
			Moneda billetesIngresados=evaCoreService.billetesIngresados(trace);
			this.LOGGER.info(trace+" "+"Obtener billetes ingresados "+billetesIngresados);
			if(billetesIngresados.getEstado()==0){
				dataBilletes=billetesIngresados.getBilletes();
				for (Moneda moneda : dataBilletes) {
					MONTO_BILLETES+=moneda.getCantidad() * moneda.getDenominacion();
				}
				/*if(MONTO_BILLETES!=TEMP_MONTO_BILLETES){
					isBilleteIngresado=true;
				}*/
				TEMP_MONTO_BILLETES=MONTO_BILLETES;
				if(Boolean.parseBoolean(billetesIngresados.getIdNote())){
					ingresandoBillete=1;
				}
				else{
					ingresandoBillete=0;
				}
				data.put("billetes", dataBilletes);
				
			}
			else{
				
			}
		} catch (Exception e) {
			/*data.put("estado", "-1");
			data.put("descripcion", "Error al obtener los billtes");
			data.put("monto", "0.0");*/
			LOGGER.error("Error obtenerBilletesIngresados ", e);
		}
		
		this.LOGGER.info(trace+" "+"resultado obtenerBilletesIngresados "+data);
		return data;
	}
}

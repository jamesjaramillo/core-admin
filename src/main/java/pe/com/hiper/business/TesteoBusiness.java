package pe.com.hiper.business;

import java.util.HashMap;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import pe.com.hiper.common.Constantes;
import pe.com.hiper.service.HCService;
import pe.com.hiper.model.Moneda;
import pe.com.hiper.common.UtilMoneda;
import pe.com.hiper.service.DeviceService;
import pe.com.hiper.service.EvaCoreService;


/**
	* Testeo de dispositivos
	* @author jamesJH
*/
@Component
public class TesteoBusiness {

    @Autowired
	EvaCoreService evaCoreService;

    @Autowired
	DeviceService deviceService;

    @Autowired
	HCService hcService;

    @Value("${kiosco.terminal}")
	private String nroTerminal;

    @Value("${kiosco.moneda}")
	private String monedaPais;

    private static final Logger LOGGER = LogManager.getLogger(HostClienteBusiness.class);

    public HashMap<String, Object> ingresarBilletesTest(String trace) {

		HashMap<String, Object> rpta = new HashMap<>();
	
	try{
		Moneda mon = this.evaCoreService.aceptarBilletes(trace);
        LOGGER.info(trace + " INICIO ingresarBilletesTest");

		if(mon.getEstado() == 0){
			String tramaCeroStackers = UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(), deviceService.obtenerDispositivos("PY01"));
			String tramaCeroCashBox = UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(), deviceService.obtenerDispositivos("CB01"));		
			
			ArrayList<HashMap<String, Integer>> billetes = new ArrayList<>();
			for(Moneda bill : mon.getBilletes()){
				HashMap<String, Integer> datos = new HashMap<>();	
				datos.put("denominacion", bill.getDenominacion());
				datos.put("cantidad", bill.getCantidad());
				billetes.add(datos);

				String billIngStacker = String.valueOf(bill.getDenominacion()*100)+";"+String.valueOf(bill.getCantidadPayout());
				String billIngCashBox = String.valueOf(bill.getDenominacion()*100)+";"+String.valueOf(bill.getCantidadCashbox());
				
				tramaCeroStackers = tramaCeroStackers.replace(String.valueOf(bill.getDenominacion()*100)+";0", billIngStacker);
				tramaCeroCashBox = tramaCeroCashBox.replace(String.valueOf(bill.getDenominacion()*100)+";0", billIngCashBox);
		
			}

			rpta.put("estado", mon.getEstado());
			rpta.put("descripcion", mon.getDescripcion());
			rpta.put("billetes", billetes);

			HashMap<String, String>  jsonRecarga = this.hcService.pruebaRecargaDescarga("", "", tramaCeroStackers, tramaCeroCashBox, "", "-", "2", "00", "00", trace, nroTerminal, "01");
			if(jsonRecarga.containsKey("estado") && jsonRecarga.get("estado").equals("0") && jsonRecarga.containsKey("print_data")){
				//LOGGER.info(trace+" "+"ingresarBilletesTest :: Se grabaron las cantidades en BD");
				System.out.println("**********" + trace+" "+"ingresarBilletesTest :: Se grabaron las cantidades en BD");
			}else{
				rpta.put("BD", "ingresarBilletesTest: No se pudo actualizar las cantidades en BD");
			}
		}else{
			rpta.put("estado", mon.getEstado());
			rpta.put("descripcion", mon.getDescripcion());
		}
			this.evaCoreService.finalizarScrow(trace, false);
		}catch (Exception e){
			rpta.put("estado", -1);
			rpta.put("descripcion","Error general");
			e.printStackTrace();
		}
	
		return rpta;
	
	}

	public HashMap<String, Object> dispensarBilletesTest(String trace, Integer denominacion, Integer cantidad) {

		HashMap<String, Object> rpta = new HashMap<>();
	
	try{
        LOGGER.info(trace + " INICIO dispensarBilletesTest - _PARAMS_: denominacion: "+denominacion+" cantidad: "+cantidad);

		ArrayList<Moneda> lsMonedas = new ArrayList<>();
		lsMonedas.add(new Moneda(denominacion, cantidad));
		Moneda mon = this.evaCoreService.dispensarBilletes(trace, lsMonedas);
		
		if(mon.getEstado() == 0){

			ArrayList<HashMap<String, Integer>> billetes = new ArrayList<>();
			for(Moneda bill : mon.getBilletes()){
				HashMap<String, Integer> datos = new HashMap<>();	
				datos.put("denominacion", bill.getDenominacion());
				datos.put("cantidad", bill.getCantidad());
				billetes.add(datos);
			}

			rpta.put("estado", mon.getEstado());
			rpta.put("descripcion", mon.getDescripcion());
			rpta.put("billetes", billetes);

			String tramaCero = UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(), deviceService.obtenerDispositivos("PY01"));

			String billDisp = String.valueOf(denominacion*100)+";"+String.valueOf(cantidad);
			tramaCero = tramaCero.replace(String.valueOf(denominacion*100)+";0", billDisp);
			HashMap<String, String>  jsonRecarga = this.hcService.pruebaRecargaDescarga("", "", tramaCero, "", "", "-", "2", "00", "00", trace, nroTerminal, "03");
	
			if(jsonRecarga.containsKey("estado") && jsonRecarga.get("estado").equals("0") && jsonRecarga.containsKey("print_data")){
				//LOGGER.info(trace+" "+"dispensarBilletes :: Se grabaron las cantidades en BD");
				System.out.println("**********" + trace+" "+"dispensarBilletes :: Se grabaron las cantidades en BD");
			}else{
				rpta.put("BD", "dispensarBilletes: No se pudo actualizar las cantidades en BD");
			}
		}else{
			rpta.put("estado", mon.getEstado());
			rpta.put("descripcion", mon.getDescripcion());
		}
			this.evaCoreService.finalizarScrow(trace, false);
		}catch (Exception e){
			rpta.put("estado", -1);
			rpta.put("descripcion","Error general");
			e.printStackTrace();
		}
	
		return rpta;
	
	}

	public HashMap<String, String> habilitarIngresoMonedaTest(String trace){
        HashMap<String, String> rpta = new HashMap<>();
        LOGGER.info(trace + " INICIO habilitarIngresoMonedaTest");
        try{
            rpta = evaCoreService.serviciosMonedero(trace, Constantes.EVACORE_MONEDERO_INICIAR);
            LOGGER.info(trace + " OUTPUT habilitarIngresoMonedaTest ", rpta.toString());
        }catch (Exception e){
            rpta.put("estado", "-1");
            rpta.put("descripcion","Error general");
            LOGGER.error(trace+" "+"ERROR GENERAL :: habilitarIngresoMonedaTest", e);
            e.printStackTrace();
        }
        return rpta;
	}

    public HashMap<String, String> getDenominacionMonedas(String trace){
        HashMap<String, String> rpta = new HashMap<>();
        LOGGER.info(trace + " INICIO getDenominacionMonedas");
        try{
            rpta = evaCoreService.serviciosMonedero(trace, Constantes.EVACORE_MONEDERO_DENOMINACIO);
            LOGGER.info(trace + " OUTPUT getDenominacionMonedas ", rpta.toString());

        }catch (Exception e){
            rpta.put("estado", "-1");
            rpta.put("descripcion","Error general");
            LOGGER.error(trace+" "+"ERROR GENERAL :: getDenominacionMonedas", e);
            e.printStackTrace();
        }
        return rpta;
	}

    public HashMap<String, String> detenerMonederoTest(String trace){
        HashMap<String, String> rpta = new HashMap<>();
        LOGGER.info(trace + " INICIO detenerMonederoTest");
        try{
            rpta = evaCoreService.serviciosMonedero(trace, Constantes.EVACORE_MONEDERO_DETENER);
            LOGGER.info(trace + " OUTPUT detenerMonederoTest ", rpta.toString());

        }catch (Exception e){
            rpta.put("estado", "-1");
            rpta.put("descripcion","Error general");
            LOGGER.error(trace+" "+"ERROR GENERAL :: detenerMonederoTest", e);
            e.printStackTrace();
        }
    
        return rpta;
	}

    public HashMap<String, String> aceptarMonedasScrowTest(String trace, String trama) {
        HashMap<String, String> rpta = new HashMap<>();
        LOGGER.info(trace + " INICIO aceptarMonedasScrowTest");
        try{
            rpta = evaCoreService.serviciosMonedero(trace, Constantes.EVACORE_ESCROW_ACEPTAR);
            LOGGER.info(trace + " OUTPUT aceptarMonedasScrowTest ", rpta.toString());
            
            if(rpta.get("estado").equals("1") && trama.length() > 0){
                String[] monedas = trama.split(":");
                String tramaCero = UtilMoneda.getFormatMonedas(new HashMap<Double, Integer>(), deviceService.obtenerDispositivos("SH01"));

                for(String mon: monedas){
                    if(mon.length() > 0){
                        String[] datos = mon.split(";");
                        tramaCero = tramaCero.replace(datos[0]+";0", mon);

                        if(Integer.parseInt(datos[1])>0)
                            evaCoreService.agregarMonedasSH(trace, Integer.parseInt(datos[1]), formatoDenominacion(datos[0]));
                    }
                }

                HashMap<String, String>  jsonRecarga = this.hcService.pruebaRecargaDescarga("", tramaCero, "", "", "", "-", "2", "00", "00", trace, nroTerminal, "01");
                if(jsonRecarga.containsKey("estado") && jsonRecarga.get("estado").equals("0") && jsonRecarga.containsKey("print_data")){
                    LOGGER.info(trace+" "+"aceptarMonedasScrowTest :: Se grabaron las cantidades en BD");
                }else{
                    LOGGER.error(trace+" "+"aceptarMonedasScrowTest :: Error al grabar las cantidades en BD");
                }
            }
        }catch (Exception e){
            rpta.put("estado", "-1");
            rpta.put("descripcion","Error general");
            LOGGER.error(trace+" "+"ERROR GENERAL :: aceptarMonedasScrowTest", e);
            e.printStackTrace();
        }
    
        return rpta;
        
        }

        public HashMap<String, String> devolverMonedasScrowTest(String trace, String trama) {
            HashMap<String, String> rpta = new HashMap<>();
            LOGGER.info(trace + " INICIO devolverMonedasScrowTest");
            try{
                rpta = evaCoreService.serviciosMonedero(trace, Constantes.EVACORE_ESCROW_DEVOLVER);
                LOGGER.info(trace + " OUTPUT devolverMonedasScrowTest ", rpta.toString());

                if(rpta.get("estado").equals("1") && trama.length()>0){
                    String[] monedas = trama.split(":");
                    String tramaCero = UtilMoneda.getFormatMonedas(new HashMap<Double, Integer>(), deviceService.obtenerDispositivos("CB02")); 
                    for(String mon : monedas){
                        if(mon.length()>0){
                            String datos[] = mon.split(";");
                            tramaCero = tramaCero.replace(datos[0]+";0", mon);
                        }
                    }

                    HashMap<String, String>  jsonRecarga = this.hcService.pruebaRecargaDescarga("", "", "", "", tramaCero,"-", "2", "00", "00", trace, nroTerminal, "01");
                    if(jsonRecarga.containsKey("estado") && jsonRecarga.get("estado").equals("0") && jsonRecarga.containsKey("print_data")){
                        LOGGER.info(trace+" "+"devolverMonedasScrowTest :: Se grabaron las cantidades en BD");
                    }else{
                        LOGGER.error(trace+" "+"devolverMonedasScrowTest :: Error al grabar las cantidades en BD");
                    }
                    
                }
            }catch (Exception e){
                rpta.put("estado", "-1");
                rpta.put("descripcion","Error general");
                LOGGER.error(trace+" "+"ERROR GENERAL :: devolverMonedasScrowTest", e);
                e.printStackTrace();
            }
        
            return rpta;
            
            }

    private double formatoDenominacion(String denominacion) {
        Double denoInt = Double.parseDouble(denominacion) / 100;

        if (monedaPais.equals("PEN")) {
            return denoInt;
        } else if (monedaPais.equals("COP")) {
            denoInt = denoInt / 100;
            return denoInt;
        } else {
            return denoInt;
        }
    }


}

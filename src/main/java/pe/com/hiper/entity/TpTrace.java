package pe.com.hiper.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tptrace")
public class TpTrace implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7544743708105459219L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idtrace")
	private Long idTrace;	
	
	@Column(name = "ctrace", nullable = false)
	private String cTrace;
	
	public Long getIdTrace() {
		return idTrace;
	}
	public void setIdTrace(Long idTrace) {
		this.idTrace = idTrace;
	}
	public String getcTrace() {
		return cTrace;
	}
	public void setcTrace(String cTrace) {
		this.cTrace = cTrace;
	}
	
	
}

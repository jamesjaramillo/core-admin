package pe.com.hiper.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tpmontopagado")
public class TpMontoPagado implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6129861212781280847L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idmontopagado")
	private Long idMontoPagado;
	
	@Column(name = "ctrace", nullable = false)
	private String cTrace;
	
	@Column(name = "ndenominacion", nullable = false)
	private double nDenominacion;
	
	@Column(name = "ntipo", nullable = false)
	private int nTipo;
	
	@Column(name = "bpayout", nullable = false)
	private int bpayout;

	public Long getIdMontoPagado() {
		return idMontoPagado;
	}

	public void setIdMontoPagado(Long idMontoPagado) {
		this.idMontoPagado = idMontoPagado;
	}

	public String getcTrace() {
		return cTrace;
	}

	public void setcTrace(String cTrace) {
		this.cTrace = cTrace;
	}

	public double getnDenominacion() {
		return nDenominacion;
	}

	public void setnDenominacion(double nDenominacion) {
		this.nDenominacion = nDenominacion;
	}

	public int getnTipo() {
		return nTipo;
	}

	public void setnTipo(int nTipo) {
		this.nTipo = nTipo;
	}

	public int getBpayout() {
		return bpayout;
	}

	public void setBpayout(int bpayout) {
		this.bpayout = bpayout;
	}
	
	

}

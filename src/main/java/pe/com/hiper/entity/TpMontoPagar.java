package pe.com.hiper.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tpmontopagar")
public class TpMontoPagar implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1664473739072771673L;

	@Id
	@Column(name = "ctrace")
	private String cTrace;
	
	@Column(name = "ncostoservicio")
	private double nCostoServicio;
	
	@Column(name = "ccodcliente")
	private String cCodCliente;
	
	@Column(name = "dfechaingreso")
	private Date dFechaIngreso;
	
	@Column(name = "ncodproducto")
	private String nCodProducto;

	public String getcTrace() {
		return cTrace;
	}

	public void setcTrace(String cTrace) {
		this.cTrace = cTrace;
	}

	public double getnCostoServicio() {
		return nCostoServicio;
	}

	public void setnCostoServicio(double nCostoServicio) {
		this.nCostoServicio = nCostoServicio;
	}

	public String getcCodCliente() {
		return cCodCliente;
	}

	public void setcCodCliente(String cCodCliente) {
		this.cCodCliente = cCodCliente;
	}

	public Date getdFechaIngreso() {
		return dFechaIngreso;
	}

	public void setdFechaIngreso(Date dFechaIngreso) {
		this.dFechaIngreso = dFechaIngreso;
	}

	public String getnCodProducto() {
		return nCodProducto;
	}

	public void setnCodProducto(String nCodProducto) {
		this.nCodProducto = nCodProducto;
	}

	
	

}

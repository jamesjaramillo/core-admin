package pe.com.hiper.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tptransactionlog")
public class TpTransactionLog implements Serializable {
	
	/**
	 * 
	 */
	
	private static final long serialVersionUID = -7670256963419258807L;
	
	@Id
	@Column(name = "ntxgencounter")
	private Long nTxGenCounter;
	
	@Column(name = "ctxmerchantid")
	private String cTxMerchantId;
	
	@Column(name = "ctxtxnnumber")
	private String cTxTxnNumber;
	
	@Column(name = "ctxterminalserial")
	private String cTxTerminalSerial;
	
	@Column(name = "ctxtype")
	private String cTxType;
	
	@Column(name = "ntxcardnumber2")
	private String nTxCardNumber2;

	public String getcTxMerchantId() {
		return cTxMerchantId;
	}

	public void setcTxMerchantId(String cTxMerchantId) {
		this.cTxMerchantId = cTxMerchantId;
	}

	public String getcTxTxnNumber() {
		return cTxTxnNumber;
	}

	public void setcTxTxnNumber(String cTxTxnNumber) {
		this.cTxTxnNumber = cTxTxnNumber;
	}

	public String getcTxTerminalSerial() {
		return cTxTerminalSerial;
	}

	public void setcTxTerminalSerial(String cTxTerminalSerial) {
		this.cTxTerminalSerial = cTxTerminalSerial;
	}

	public String getcTxType() {
		return cTxType;
	}

	public void setcTxType(String cTxType) {
		this.cTxType = cTxType;
	}

	public String getnTxCardNumber2() {
		return nTxCardNumber2;
	}

	public void setnTxCardNumber2(String nTxCardNumber2) {
		this.nTxCardNumber2 = nTxCardNumber2;
	}

	
	
	
	

}

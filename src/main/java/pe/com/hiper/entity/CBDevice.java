package pe.com.hiper.entity;

import java.sql.Connection; 
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class CBDevice {
    
	   private String M_cDsId;
	   private String M_cDsTerminal;
	   private String M_cDsStatusDevice;
	   private String M_cDsDeviceCode;
	   private String M_dDsDeviceName;
	   private String M_cDsMoneyCode;
	   private String M_cDsCodeDenomination;
	   private String M_cDsIniDenomination;
	   private String M_nDsQuantityDenomination;
	   private String M_cDsLogicalStatus;
	   private Date M_fDsDateIniDenomination;
	   private Date M_fDsDateQuantityDenomination;

	   private static final String queryTmDevice ="SELECT cDsId , cDsTerminal ,cDsStatusDevice,cDsDeviceCode,dDsDeviceName,cDsMoneyCode,cDsCodeDenomination,cDsIniDenomination,"
	           + "nDsQuantityDenomination,cDsLogicalStatus,fDsDateIniDenomination " +
	                                    ",fDsDateQuantityDenomination  FROM tmDevice "; 
	   
	    public String getM_cDsId() {
	        return M_cDsId;
	    }

	    public void setM_cDsId(String M_cDsId) {
	        this.M_cDsId = M_cDsId;
	    }

	    public String getM_cDsTerminal() {
	        return M_cDsTerminal;
	    }

	    public void setM_cDsTerminal(String M_cDsTerminal) {
	        this.M_cDsTerminal = M_cDsTerminal;
	    }

	    public String getM_cDsStatusDevice() {
	        return M_cDsStatusDevice;
	    }

	    public void setM_cDsStatusDevice(String M_cDsStatusDevice) {
	        this.M_cDsStatusDevice = M_cDsStatusDevice;
	    }

	    public String getM_cDsDeviceCode() {
	        return M_cDsDeviceCode;
	    }

	    public void setM_cDsDeviceCode(String M_cDsDeviceCode) {
	        this.M_cDsDeviceCode = M_cDsDeviceCode;
	    }

	    public String getM_dDsDeviceName() {
	        return M_dDsDeviceName;
	    }

	    public void setM_dDsDeviceName(String M_dDsDeviceName) {
	        this.M_dDsDeviceName = M_dDsDeviceName;
	    }

	    public String getM_cDsMoneyCode() {
	        return M_cDsMoneyCode;
	    }

	    public void setM_cDsMoneyCode(String M_cDsMoneyCode) {
	        this.M_cDsMoneyCode = M_cDsMoneyCode;
	    }

	    public String getM_cDsCodeDenomination() {
	        return M_cDsCodeDenomination;
	    }

	    public void setM_cDsCodeDenomination(String M_cDsCodeDenomination) {
	        this.M_cDsCodeDenomination = M_cDsCodeDenomination;
	    }

	    public String getM_cDsIniDenomination() {
	        return M_cDsIniDenomination;
	    }

	    public void setM_cDsIniDenomination(String M_cDsIniDenomination) {
	        this.M_cDsIniDenomination = M_cDsIniDenomination;
	    }

	    public String getM_nDsQuantityDenomination() {
	        return M_nDsQuantityDenomination;
	    }

	    public void setM_nDsQuantityDenomination(String M_nDsQuantityDenomination) {
	        this.M_nDsQuantityDenomination = M_nDsQuantityDenomination;
	    }

	    public String getM_cDsLogicalStatus() {
	        return M_cDsLogicalStatus;
	    }

	    public void setM_cDsLogicalStatus(String M_cDsLogicalStatus) {
	        this.M_cDsLogicalStatus = M_cDsLogicalStatus;
	    }

	    public Date getM_fDsDateIniDenomination() {
	        return M_fDsDateIniDenomination;
	    }

	    public void setM_fDsDateIniDenomination(Date M_fDsDateIniDenomination) {
	        this.M_fDsDateIniDenomination = M_fDsDateIniDenomination;
	    }

	    public Date getM_fDsDateQuantityDenomination() {
	        return M_fDsDateQuantityDenomination;
	    }

	    public void setM_fDsDateQuantityDenomination(Date M_fDsDateQuantityDenomination) {
	        this.M_fDsDateQuantityDenomination = M_fDsDateQuantityDenomination;
	    }
	    
	    /*public static ArrayList<CBDevice> obtenerDenominacionesDispositivo(String terminal,String dispositivo, Connection conn){

	        ArrayList<CBDevice> lstDevice= new ArrayList<CBDevice>();
	        CBDevice device=null;        
	            if ((terminal == null)) {
	                return lstDevice=null;
	            }
	            try {
	                Statement st = conn.createStatement();
	                String szQuery=queryTmDevice+" WHERE cDsTerminal='"+terminal+"' AND cDsLogicalStatus='0'"
	                        + " ORDER BY cDsId DESC";
	                ResultSet rs = st.executeQuery(szQuery);            
	                        
	                while (rs.next()) {
	                    device=new CBDevice();   
	                    if(rs.getString("cDsDeviceCode")!=null && rs.getString("cDsDeviceCode").substring(0,2).equals(dispositivo)){
	                        device.setM_cDsId(rs.getString("cDsId"));
	                        device.setM_cDsTerminal(rs.getString("cDsTerminal"));
	                        device.setM_cDsStatusDevice(rs.getString("cDsStatusDevice"));
	                        device.setM_cDsDeviceCode(rs.getString("cDsDeviceCode"));
	                        device.setM_dDsDeviceName(rs.getString("dDsDeviceName"));
	                        device.setM_cDsMoneyCode(rs.getString("cDsMoneyCode"));
	                        device.setM_cDsCodeDenomination(rs.getString("cDsCodeDenomination"));
	                        device.setM_cDsIniDenomination(rs.getString("cDsIniDenomination"));
	                        device.setM_nDsQuantityDenomination(rs.getString("nDsQuantityDenomination"));
	                        device.setM_cDsLogicalStatus(rs.getString("cDsLogicalStatus"));
	                        device.setM_fDsDateIniDenomination(rs.getDate("fDsDateIniDenomination"));
	                        device.setM_fDsDateQuantityDenomination(rs.getDate("fDsDateQuantityDenomination"));
	                        lstDevice.add(device);
	                    }
	                }
	                st.close();
	                return lstDevice;
	            }catch(Exception ex){
	                lstDevice = new ArrayList<CBDevice>();
	                System.out.println("ERROR.obtenerDenominacionesDispositivo: "+ex.getMessage());
	                return lstDevice;
	            }
	        }*/
	    
	    
	    /*public boolean actualizaContadores(String idDispositivo, String quantity, Connection conn) {
	        boolean resultado = false;
	        try {
	            Statement st = conn.createStatement();
	            String szQuery = "UPDATE TMDEVICE " +
	                            "SET nDsQuantityDenomination='"+quantity+"',"
	                          + "fDsDateQuantityDenomination='"+Funciones.get_FechaActual_AAAAMMDD().toString()+"' WHERE cDsId='"+idDispositivo+"'";            
	            st.executeUpdate(szQuery);
	            st.close();
	            resultado=true;
	            return resultado;
	        } catch (Exception sqle) {
	        	System.out.println("ERROR EN BASE DE DATOS TERMINAL: " + sqle.getMessage());
	            return resultado;
	        }
	    }*/
}

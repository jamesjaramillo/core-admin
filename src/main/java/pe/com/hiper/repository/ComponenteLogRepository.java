package pe.com.hiper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.hiper.entity.TpComponenteLog;

@Repository
public interface ComponenteLogRepository extends JpaRepository<TpComponenteLog, Long> {

}

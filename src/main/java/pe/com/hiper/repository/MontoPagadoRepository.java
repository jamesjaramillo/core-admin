package pe.com.hiper.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.hiper.entity.TpMontoPagado;
/**
 * 
 * @author rrondinel
 *
 */

@Repository
public interface MontoPagadoRepository extends JpaRepository<TpMontoPagado, Long> {

}

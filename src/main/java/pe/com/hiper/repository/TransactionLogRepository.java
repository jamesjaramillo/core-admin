package pe.com.hiper.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pe.com.hiper.entity.TpTransactionLog;


@Repository
public interface TransactionLogRepository extends JpaRepository<TpTransactionLog, Long> {
	
	@Transactional
	@Modifying
	@Query("update TpTransactionLog set nTxCardNumber2 = :formatoVuelto where cTxTerminalSerial = :terminal and cTxTxnNumber = :trace and cTxType IN :lista")
	public void actualizarMonedasIngresada(@Param("trace") String trace, @Param("terminal") String terminal, @Param("formatoVuelto") String formatoVuelto, @Param("lista") List<String> lista);
}

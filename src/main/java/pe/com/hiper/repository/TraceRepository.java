package pe.com.hiper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.hiper.entity.TpTrace;

@Repository
public interface TraceRepository extends JpaRepository<TpTrace, Long> {

}

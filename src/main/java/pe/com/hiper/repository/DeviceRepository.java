package pe.com.hiper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pe.com.hiper.entity.TmDevice;

@Repository
public interface DeviceRepository extends JpaRepository<TmDevice, Long> {
	
	@Modifying
	@Query("update TmDevice set nDsQuantityDenomination = nDsQuantityDenomination + :cantidad where cDsCodeDenomination = :denominacion and cDsDeviceCode = :device")
	void actualizarCantidadDenominacion(@Param("cantidad") int cantidad, @Param("denominacion") String denominacion, @Param("device") String device);

}

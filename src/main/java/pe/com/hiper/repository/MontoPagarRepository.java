package pe.com.hiper.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.hiper.entity.TpMontoPagar;
/**
 * 
 * @author rrondinel
 *
 */
@Repository
public interface MontoPagarRepository extends JpaRepository<TpMontoPagar, String> {

}

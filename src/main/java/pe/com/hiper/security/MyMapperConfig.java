package pe.com.hiper.security;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyMapperConfig {
	
	 @Bean
	    public ModelMapper modelMapper() {
	       ModelMapper modelMapper = new ModelMapper();
	       return modelMapper;
	    }

}

package pe.com.hiper.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.hiper.entity.TpTrace;
import pe.com.hiper.repository.TraceRepository;
import pe.com.hiper.service.TraceService;

@Service
public class TraceServiceImpl implements TraceService {
	
	@Autowired
	private TraceRepository repository;

	@Override
	public TpTrace save(TpTrace trace) {
		// TODO Auto-generated method stub
		return repository.save(trace);
	}

	@Override
	public TpTrace getTrace() {
		// TODO Auto-generated method stub
		TpTrace tpTrace=repository.findAll().get(0);
		Long nro=new Long(tpTrace.getcTrace());
		tpTrace.setcTrace(String.format("%06d", (nro+1)));
		repository.save(tpTrace);
		return tpTrace;
	}

}

package pe.com.hiper.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.hiper.entity.TpMontoPagar;
import pe.com.hiper.repository.MontoPagarRepository;
import pe.com.hiper.service.MontoPagarService;

@Service
public class MontoPagarServiceImpl implements MontoPagarService {

	@Autowired
	MontoPagarRepository montoPagarRepository;
	
	@Override
	public TpMontoPagar getMontoPagar(String trace) {
		// TODO Auto-generated method stub
		return montoPagarRepository.findById(trace).orElse(null);
	}

	@Override
	public TpMontoPagar saveMontoPagar(TpMontoPagar montoPagar) {
		// TODO Auto-generated method stub
		return montoPagarRepository.save(montoPagar);
	}

}

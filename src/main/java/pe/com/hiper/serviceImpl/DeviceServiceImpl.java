package pe.com.hiper.serviceImpl;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.com.hiper.entity.TmDevice;
import pe.com.hiper.repository.DeviceRepository;
import pe.com.hiper.service.DeviceService;

@Service
public class DeviceServiceImpl implements DeviceService {

	@Autowired
	DeviceRepository deviceRepository;

	@Override
	public List<TmDevice> obtenerDispositivos(String dispositivo) {
		// TODO Auto-generated method stub
		TmDevice tmDevice = new TmDevice();
		tmDevice.setcDsDeviceCode(dispositivo);
		tmDevice.setcDsLogicalStatus("0");

		return deviceRepository.findAll(Example.of(tmDevice)).stream()
				.sorted(Comparator.comparing(TmDevice::getcDsMoneyCode).reversed()).collect(Collectors.toList());
	}

	@Override
	public TmDevice actualizarCantidad(TmDevice device) {
		// TODO Auto-generated method stub
		return deviceRepository.save(device);
	}

	@Override
	public void actualizarCantidadDenominacion(int cantidad, String denominacion, String device) {
		// TODO Auto-generated method stub
		System.out.println("cantidad -> "+cantidad+"\t denominacion -> "+denominacion+"\t device -> "+device);
		deviceRepository.actualizarCantidadDenominacion(cantidad, denominacion, device);
	}

	@Override
	public TmDevice getDenominacionDispositivo(String deniminacion, String device) {
		// TODO Auto-generated method stub
		TmDevice tmDevice = new TmDevice();
		tmDevice.setcDsCodeDenomination(deniminacion);
		tmDevice.setcDsDeviceCode(device);
		tmDevice.setcDsLogicalStatus("0");
		return deviceRepository.findOne(Example.of(tmDevice)).get();
	}

}

package pe.com.hiper.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.hiper.repository.TransactionLogRepository;
import pe.com.hiper.service.TransactionLogService;

@Service
public class TransactionLogServiceImpl implements TransactionLogService {

	@Autowired
	TransactionLogRepository transactionLogRepo;
	
	@Override
	public void actualizarMonedasIngresada(String trace, String terminal, String formatoMonedas) {
		// TODO Auto-generated method stub
		System.out.println("trace -> "+trace+"\t terminal -> "+terminal+"\t formatoMonedas -> "+formatoMonedas);
		List<String> lista=new ArrayList<String>();
		lista.add("21");
		lista.add("30");
		lista.add("08");
		transactionLogRepo.actualizarMonedasIngresada(trace, terminal, formatoMonedas, lista);
	}
	
}

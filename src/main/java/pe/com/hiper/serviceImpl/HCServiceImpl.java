package pe.com.hiper.serviceImpl;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.TpComponenteLog;
import pe.com.hiper.service.ComponenteLogService;
import pe.com.hiper.service.HCService;
/**
 * 
 * @author rrondinel
 *
 */

@Service
public class HCServiceImpl implements HCService {
	private static final Logger LOGGER = LogManager.getLogger(HCServiceImpl.class);
	DecimalFormat formatter = new DecimalFormat("#0.00");

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Value("${url.api.hc}")
	private String urlApi;

	@Value("${hcenter.aplicacion}")
	private String aplHcenter;

	@Autowired
	private ComponenteLogService componenteService;

	@Override
	public HashMap<String, String> obtenerCostoProducto(String trace, String codigoServicio, String nroCupon) {
		LOGGER.info(trace + " INICIO obtenerCostoProducto");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "obtenerCostoProducto");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("transaction_code", "TRX_DEUDA_PORTALES");
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("numero_ticket", codigoServicio);
			jsonRequest.put("codigoCupon", nroCupon);
			jsonRequest.put("data", "transaction_code;aplicacion_trx;numero_ticket;codigoCupon");
			jsonRequest.put("terminal_serial_number", nroTerminal);
			jsonRequest.put("trace_number", trace);

			LOGGER.info(trace + " INPUT obtenerCostoProducto " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if(!data.isEmpty()) {
				data.put("estado", "0");
			}
			else {
				data.put("estado", "1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT obtenerCostoProducto " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR obtenerCostoProducto", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> procesarPagoTotalNuevo(double costo_servicio, double monto_pagado,
			String id_producto, String id_cliente, String deno_cant_bill, String deno_cant_mone, String trace,
			String estado_hopper1, String estado_hopper2, String estado_hopper3, String desc_producto, String agencia,
			String terminal, String flagTPP, String flagRecicla, String deno_cant_bill_pay, String camino,
			String numOperacion, String numAutorizacion, String cardNumberEnc, String PaymentTypeCode) {
		LOGGER.info(trace + " INICIO procesarPagoTotalNuevo");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "procesarPagoTotalNuevo");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("transaction_code","TRX_PAGO_TOTAL");
//			jsonRequest.put("aplicacion_trx","SWITCH");
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("service_code",id_producto);
			jsonRequest.put("desc_service",desc_producto);
			jsonRequest.put("monto_ingresado",formatter.format(monto_pagado).replace(",", "."));
			jsonRequest.put("monto_maximo",formatter.format(costo_servicio).replace(",", "."));
			jsonRequest.put("calculaVueltoHC","0");
			jsonRequest.put("agencia",agencia);
			jsonRequest.put("cashbox",deno_cant_bill);
			jsonRequest.put("payout",deno_cant_bill_pay);
			jsonRequest.put("smartHopper",deno_cant_mone);
			jsonRequest.put("cod_user",id_cliente);
			jsonRequest.put("estado_hopper1",estado_hopper1);
			jsonRequest.put("estado_hopper2",estado_hopper2);
			jsonRequest.put("estado_hopper3",estado_hopper3);
			jsonRequest.put("cabecera_voucher", "");//upc
			jsonRequest.put("desc_servicio_voucher","PRODUCTO");
			jsonRequest.put("cliente_voucher","CLIENTE");
			jsonRequest.put("desc_costo_voucher","TOTAL A PAGAR");
			jsonRequest.put("desc_pago_voucher","MONTO PAGADO");
			jsonRequest.put("num_tarjeta",id_cliente);
			jsonRequest.put("num_tarjeta_orig","1234565678");
			jsonRequest.put("monto_recarga",formatter.format(monto_pagado).replace(",", "."));
			jsonRequest.put("flagTPP",flagTPP);
			jsonRequest.put("flagRecicla",flagRecicla);
			jsonRequest.put("flag_cambio", camino);
			
			if(numOperacion.equals("") && numAutorizacion.equals("")){
				jsonRequest.put("data","transaction_code;aplicacion_trx;service_code;desc_service;monto_ingresado;monto_maximo;agencia;cashbox;payout;smartHopper;cod_user;estado_hopper1;estado_hopper2;estado_hopper3;cabecera_voucher;desc_servicio_voucher;cliente_voucher;desc_costo_voucher;desc_pago_voucher;num_tarjeta;num_tarjeta_orig;monto_recarga;flagTPP;flagRecicla;calculaVueltoHC;flag_cambio");
			}else{
				jsonRequest.put("numOperacion",numOperacion);
				jsonRequest.put("numAutorizacion",numAutorizacion);
				jsonRequest.put("cardNumberEnc",cardNumberEnc);
				jsonRequest.put("PaymentTypeCode",PaymentTypeCode);
				
				jsonRequest.put("data","transaction_code;aplicacion_trx;service_code;desc_service;monto_ingresado;monto_maximo;agencia;cashbox;payout;smartHopper;cod_user;estado_hopper1;estado_hopper2;estado_hopper3;cabecera_voucher;desc_servicio_voucher;cliente_voucher;desc_costo_voucher;desc_pago_voucher;num_tarjeta;num_tarjeta_orig;monto_recarga;flagTPP;flagRecicla;calculaVueltoHC;flag_cambio;numOperacion;numAutorizacion;cardNumberEnc;PaymentTypeCode");
			}
			
			jsonRequest.put("terminal_serial_number",terminal);
			jsonRequest.put("trace_number",trace);

			LOGGER.info(trace + " INPUT procesarPagoTotalNuevo " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if(!data.isEmpty()) {
				data.put("estado", "0");
			}
			else {
				data.put("estado", "-1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT procesarPagoTotalNuevo " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR procesarPagoTotalNuevo", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> recargarHopper(String denMonRecarga, String denMonRecargaSH, String denMonRecargaBI,
			String dataRecargaCB, String dataRecargaCBM, String cargaPapel, String opcRecaudador, String flagRetiroMon,
			String flagRetiroBill, String trace, String terminal, int tipoRecarga, String operationType, String tipodoc,
			String numdoc, String usuario) {
		LOGGER.info(trace + " INICIO recargarHopper");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "recargarHopper");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("transaction_code","RECARGA_HOPPERS");
//			jsonRequest.put("aplicacion_trx","SWITCH");
			jsonRequest.put("aplicacion_trx",aplHcenter);
			jsonRequest.put("dataRecarga",denMonRecarga);
			jsonRequest.put("numIniHojaLaser","-");
			jsonRequest.put("cargaPapel",cargaPapel);
			jsonRequest.put("opcRecaudador",opcRecaudador);
			jsonRequest.put("flagRetiroMon",flagRetiroMon);
			jsonRequest.put("flagRetiroBill",flagRetiroBill);
			jsonRequest.put("num_referencia",trace);
			jsonRequest.put("cod_user",usuario);
			jsonRequest.put("numOperacion",usuario);
			
			if(tipoRecarga==2){
				jsonRequest.put("dataRecargaSH",denMonRecargaSH);
				jsonRequest.put("dataRecargaBI",denMonRecargaBI);
				jsonRequest.put("dataRecargaCB",dataRecargaCB);
				jsonRequest.put("dataRecargaCBM",dataRecargaCBM);
				jsonRequest.put("tipodoc",tipodoc); //nombre del rol
				jsonRequest.put("numdoc",numdoc); //numero de tarjeta
				jsonRequest.put("data","transaction_code;aplicacion_trx;dataRecarga;dataRecargaSH;dataRecargaBI;dataRecargaCB;dataRecargaCBM;numIniHojaLaser;cargaPapel;opcRecaudador;flagRetiroMon;flagRetiroBill;operationType;tipodoc;numdoc;cod_user;numOperacion");
				jsonRequest.put("operationType", operationType);
			}else{
				jsonRequest.put("data","transaction_code;aplicacion_trx;dataRecarga;numIniHojaLaser;cargaPapel;opcRecaudador;flagRetiroMon;flagRetiroBill;cod_user;numOperacion");
			}
			
			jsonRequest.put("terminal_serial_number",terminal);
			jsonRequest.put("trace_number",trace);

			LOGGER.info(trace + " INPUT procesarPagoTotalNuevo " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if(!data.isEmpty()) {
				data.put("estado", "0");
			}
			else {
				data.put("estado", "-1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT recargarHopper " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR recargarHopper", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> estadoHopSMHopBill(String trace, String opcRecaudador, String flagRetiroMon,
			String flagRetiroBill, String terminal, String nombreRol, String numTrjUsuario) {
		LOGGER.info(trace + " INICIO estadoHopSMHopBill");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "estadoHopSMHopBill");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("transaction_code","ESTADO_HOPPERS");
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("numIniHojaLaser","-");
			jsonRequest.put("cargaPapel","-");
			jsonRequest.put("opcRecaudador",opcRecaudador);
			jsonRequest.put("flagRetiroMon",flagRetiroMon);
			jsonRequest.put("flagRetiroBill",flagRetiroBill);
			jsonRequest.put("num_referencia",trace);
			jsonRequest.put("tipodoc",nombreRol); //nombre del rol
			jsonRequest.put("numdoc",numTrjUsuario); //numero de tarjeta
			jsonRequest.put("data","transaction_code;aplicacion_trx;numIniHojaLaser;cargaPapel;opcRecaudador;flagRetiroMon;flagRetiroBill;num_referencia;terminal_serial_number;trace_number;tipodoc;numdoc");
			jsonRequest.put("terminal_serial_number",terminal);
			jsonRequest.put("trace_number",trace);

			LOGGER.info(trace + " INPUT estadoHopSMHopBill " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if(!data.isEmpty()) {
				data.put("estado", "0");
			}
			else {
				data.put("estado", "-1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT estadoHopSMHopBill " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR estadoHopSMHopBill", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> reportePortales(String trxCode, String tipoTrx, int numTrx, String trace,
			String terminal, String nombreRol, String numTrjUsuario) {
		LOGGER.info(trace + " INICIO reportePortales");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "reportePortales");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("transaction_code",trxCode);
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("num_referencia",trace);
			
			if( !trxCode.equals("REPORTE_ERRORES_TRX") ){
				jsonRequest.put("lista_numero", numTrx);
				
				if( trxCode.equals("REPORTE_DIARIO") ){
					jsonRequest.put("tipodoc",nombreRol); //nombre del rol
					jsonRequest.put("numdoc",numTrjUsuario); //numero de tarjeta
					jsonRequest.put("lista_id", tipoTrx);
					jsonRequest.put("data","transaction_code;aplicacion_trx;num_referencia;lista_numero;tipodoc;numdoc;lista_id");
				}else{
					if( trxCode.equals("REPORTE_CUADRE_OPER") ){
						jsonRequest.put("tipodoc",nombreRol); //nombre del rol
						jsonRequest.put("numdoc",numTrjUsuario); //numero de tarjeta
						jsonRequest.put("data","transaction_code;aplicacion_trx;num_referencia;lista_numero;tipodoc;numdoc");
					}else{
						jsonRequest.put("data","transaction_code;aplicacion_trx;num_referencia;lista_numero");
					}
				}
			}else{
				jsonRequest.put("fecha", "");
				jsonRequest.put("data","transaction_code;aplicacion_trx;num_referencia;fecha");
			}
			
			jsonRequest.put("terminal_serial_number",terminal);
			jsonRequest.put("trace_number",trace);

			LOGGER.info(trace + " INPUT reportePortales " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if(!data.isEmpty()) {
				data.put("estado", "0");
			}
			else {
				data.put("estado", "-1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT reportePortales " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR reportePortales", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> reporteUlt5TrxPortales(String trace, String terminal, String nombreRol,
			String numTrjUsuario) {
		LOGGER.info(trace + " INICIO reporteUlt5TrxPortales");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "reporteUlt5TrxPortales");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("transaction_code","REPORTE_ULTIMASTRX");
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("num_referencia",trace);
			jsonRequest.put("numdoc",numTrjUsuario); //numero de tarjeta
			jsonRequest.put("tipodoc",nombreRol); //nombre del rol
			jsonRequest.put("data","transaction_code;aplicacion_trx;num_referencia;numdoc;tipodoc");
			jsonRequest.put("terminal_serial_number",terminal);
			jsonRequest.put("trace_number",trace);

			LOGGER.info(trace + " INPUT reporteUlt5TrxPortales " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if(!data.isEmpty()) {
				data.put("estado", "0");
			}
			else {
				data.put("estado", "-1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT reporteUlt5TrxPortales " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR reporteUlt5TrxPortales", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> pruebaRecargaDescarga(String denMonRecarga, String denMonRecargaSH, String denMonRecargaBI, String dataRecargaCB, String dataRecargaCBMonedas, String cargaPapel, String opcRecaudador, String flagRetiroMon, 
	String flagRetiroBill, String trace, String terminal, String operationType) {

		HashMap<String, String> data = new HashMap<String, String>();

		LOGGER.info(trace + " INICIO pruebaRecargaDescarga");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "pruebaRecargaDescarga");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		
		try {
	
		RestTemplate restTemplate = new RestTemplate();
		JSONObject jsonRequest = new JSONObject();

		jsonRequest.put("transaction_code","TESTEO_RECARGA");
		jsonRequest.put("aplicacion_trx",aplHcenter);
		jsonRequest.put("dataRecarga",denMonRecarga);
		jsonRequest.put("dataRecargaSH",denMonRecargaSH);
		jsonRequest.put("dataRecargaBI",denMonRecargaBI);
		jsonRequest.put("dataRecargaCB",dataRecargaCB);
		jsonRequest.put("dataRecargaCBM",dataRecargaCBMonedas);
		jsonRequest.put("operationType", operationType);
		jsonRequest.put("numIniHojaLaser","-");
		jsonRequest.put("cargaPapel",cargaPapel);
		jsonRequest.put("opcRecaudador",opcRecaudador);
		jsonRequest.put("flagRetiroMon",flagRetiroMon);
		jsonRequest.put("flagRetiroBill",flagRetiroBill);
		jsonRequest.put("num_referencia",trace);
		jsonRequest.put("data","transaction_code;aplicacion_trx;dataRecarga;dataRecargaSH;dataRecargaBI;dataRecargaCB;dataRecargaCBM;numIniHojaLaser;cargaPapel;opcRecaudador;flagRetiroMon;flagRetiroBill;operationType");
		
		jsonRequest.put("terminal_serial_number",terminal);
		jsonRequest.put("trace_number",trace);

		LOGGER.info(trace + " INPUT pruebaRecargaDescarga " + jsonRequest.toString());

		HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
		ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
		data = result.getBody();
		if(!data.isEmpty()) {
			data.put("estado", "0");
		}
		else {
			data.put("estado", "-1");
		}
		componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
		LOGGER.info(trace + " OUTPUT pruebaRecargaDescarga " + result.toString());
		}catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR pruebaRecargaDescarga", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		
		return data;
	
	}

}

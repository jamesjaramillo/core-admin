package pe.com.hiper.serviceImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.hiper.entity.TpMontoPagado;
import pe.com.hiper.repository.MontoPagadoRepository;
import pe.com.hiper.service.MontoPagadoService;

@Service
public class MontoPagadoServiceImpl implements MontoPagadoService {

	@Autowired
	MontoPagadoRepository montoPagadoRepository;

	@Override
	public TpMontoPagado saveMontoPagado(TpMontoPagado montoPagado) {
		// TODO Auto-generated method stub
		return montoPagadoRepository.save(montoPagado);
	}

	@Override
	public List<TpMontoPagado> listarMontoPagado(String trace) {

		return montoPagadoRepository.findAll().stream()
				.filter(p -> p.getcTrace().equals(trace))
				.collect(Collectors.toList());
		
		//return montoPagadoRepository.findAll();
	}

}
